<?php
session_start();
require_once(__DIR__.'/Source/Includes/Config.php');
Validation::Logout();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="<?= $cfg['web_author'] ?>">
    <meta name="description" content="<?= $cfg['web_desc_site'] ?>">
    <meta name="keywords" content="<?= $cfg['web_keywords'] ?>">
	
    <title><?= $cfg['web_title'] ?></title>
    <!-- Theme .css files -->
    <?php require_once('Source/Includes/StyleIncludes.php'); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <?php require_once(__DIR__.'/Source/Includes/ScriptIncludes.php'); ?>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <?php
        require_once(__DIR__.'/Source/Includes/Construct.php');
    ?>
  </body>
</html>