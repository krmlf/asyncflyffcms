<?php 

class News {
  
  private function CheckForumUrl($url){
      $rCurlHandle = curl_init($url);
      
      curl_setopt($rCurlHandle, CURLOPT_CONNECTTIMEOUT, 10);
      curl_setopt($rCurlHandle, CURLOPT_HEADER, true);
      curl_setopt($rCurlHandle, CURLOPT_NOBODY, true);
      curl_setopt($rCurlHandle, CURLOPT_RETURNTRANSFER, true);
      
      $bool = curl_exec($rCurlHandle);
      
      curl_close($rCurlHandle);
      
      if($bool){return true;}else {return false;}
   }
   
   private function ModifyString($string, $search = array()){
     $isImage = false;
     $search['image'] = null;
     $search = explode('--', $string);

     $search['text'] = null;
     foreach($search as $key => $val){
        if(preg_match('/.jpg/', $val) === 1){
          $isImage = true;
          if(preg_match('/Image: /', $val) === 1){
            $search['image'] = '<img src="http://' . str_replace('Image: ', null, $val) . '" title="" width="80%">';
            $saveImageKey = array(true, $key);
          }else {
            unset($search[$key]);
          }
        }
     }
     
     if($isImage){
       foreach($search as $key => $val){
         if($key === 'image'){
            $search['text'] .= '<br>'.$search[$key].'<br>';
          }
        }
      }
      
      if($isImage){
        foreach($search as $key => $val){
          if($key !== 'image' && $key !== $saveImageKey[1] && $key !== 'text'){
            $search['text'] .= nl2br($search[$key]);
          }
        }
      }else{
        $search['text'] = nl2br($string);
      }
      
      foreach($search as $key => $val){
        if($key !== 'text'){
          unset($search[$key]);
        }
      }
      return $search['text'];
   }
  
    public function GetTopicValues($url, $topic){
        $objNews = new DOMDocument();
        $objNews->load($topic);
        
        $news = $objNews->getElementsByTagName("item");
        if(self::CheckForumUrl($url)){
          if($news->length == 0) {
                $arr = array('danger', 'No topics found.');
                $arr['bool'] = false;
          } else {
              $arr = array();
              $object = array();
              $arr['bool'] = true;
                foreach($news as $value) {
                    $object['titles'] = $value->getElementsByTagName("title");
                    $object['title'] = $object['titles']->item(0)->nodeValue;
  
                    $object['texts'] = $value->getElementsByTagName("encoded");
                    $object['text'] = self::ModifyString($object['text']  = $object['texts']->item(0)->nodeValue);
                    
  
                    $object['links'] = $value->getElementsByTagName("link");
                    $object['link']  = $object['links']->item(0)->nodeValue;
  
                    $object['dates'] = $value->getElementsByTagName("pubDate");
                    $object['date']  = substr($object['dates']->item(0)->nodeValue, 0, -6); 
                    $arr[] = $object;
                  }              
            }
        }else {
          $arr = array('danger', 'Forum not found.');
          $arr['bool'] = false;
        }
        
        return $arr;
    }
  
}