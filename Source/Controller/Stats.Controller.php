<?php 

class Serverstats extends Database {
	
	private function GetPlayerOnlineCount(){
		$count = self::Select(array('OnlineCount'), 'Character', 'CHARACTER_TBL', array('MultiServer' => 0), null, ' COUNT([MultiServer]) as ', 1, '>');
		return $count['Result'][1]['OnlineCount'];
	}
	
	private function PingServerChannel($port, $ip){
		if(@fsockopen($ip, $port, $errno, $errstr, 0.45) >= 0.45) {
			return true;
		} 
		else {
			return false;
		}
	}
	
	private function GetServerAvailability($port, $ip, $maintenance){
		$ping = self::PingServerChannel($port, $ip);
		
		if($maintenance === 1){
			return 'Maintenance';
		}else {
			if($ping){
				return 'Online';
			}else {
				return 'Offline';
			}
		}
	}
	
	private function GetTotalOnlinePlayerAmount(){
		$amount = self::Select(array('number'), 'Log', 'LOG_USER_CNT_TBL', null, ' ORDER BY [number] DESC', 'TOP 1 ');
		return $amount['Result'][1]['number'];
	}
	
	private function CraftPlayerID($playerID){
		$responseLength = 7;
		$valueLength = strlen($playerID);
		$valueLengthExtension = null;
		
		for($i = 1;$i <= $responseLength-$valueLength;$i++){
			$valueLengthExtension .= 0;
		};
		$mPlayerID = $valueLengthExtension.$playerID;
		$player = self::Select(array('m_szName'), 'Character', 'CHARACTER_TBL', array('m_idPlayer' => $mPlayerID), null, 'TOP 1 ');
		return $player;
	}
	
	private function CraftGuildID($guildID){
		$guild = self::Select(array('m_szGuild'), 'Character', 'GUILD_TBL', array('m_idGuild' => $guildID), null, 'TOP 1 ');
		
		return $guild;
	}
	
	private function GetServerLord(){
		$lord = self::Select(array('idElection', 'idLord'), 'Character', 'tblLord', null, ' ORDER BY [idElection] DESC', 'TOP 1');
		if($lord['Rows'] > 0){
			$lord = $lord['Result'][1]['idLord'];
			$player = self::CraftPlayerID($lord);
			$player['Rows'] != null ? $player = $player['Result'][1]['m_szName'] : $player = null;
			return $player;
		}else {
			return 'None';
		}
	}
	
	private function GetGuildWarWinner(){
		$winner = self::Select(array('CombatID', 'GuildID', 'Point'), 'Character', 'tblCombatJoinGuild', array('Status' => 31), ' ORDER BY [CombatID] DESC, [Point] DESC', 'TOP 1');
		if($winner['Rows'] > 0){
			$winner = $winner['Result'][1]['GuildID'];
		
			$guild = self::CraftGuildID($winner);
			
			return $guild['Result'][1]['m_szGuild'];
		}else {
			return 'None';
		}
	}
	
	public function GetMostViolentPlayer(){
		$mvp = self::Select(array('CombatID', 'PlayerID', 'Point'), 'Character', 'tblCombatJoinPlayer', array('Status' => 30), ' ORDER BY [CombatID] DESC, [Point] DESC', 'TOP 1');
		if($mvp['Rows'] > 0){
			$mvp = $mvp['Result'][1]['PlayerID'];
		
			$player = self::CraftPlayerID(intval($mvp));
			
			return $player['Result'][1]['m_szName'];
		}else {
			return 'None';
		}
	}
	
	public function CatchSmallStats($port, $ip = '127.0.0.1', $maintenance){
		$object = array();
		//Set bool = true
		$object['bool'] = true;
		//Channel 1-1 
		$object['CheckChannel'] = self::GetServerAvailability($port, $ip, $maintenance);
		//Get Online Player
		$object['OnlinePlayer'] = self::GetPlayerOnlineCount();
		//Get Total Online Player Amount
		$object['TotalOnlinePlayerAmount'] = self::GetTotalOnlinePlayerAmount();
		//Get GuildWar Winner
		$object['GuildWarWinner'] = self::GetGuildWarWinner();
		//Get MVP
		$object['MostViolentPlayer'] = self::GetMostViolentPlayer();
		//Get Total Online Player Amount
		$object['ServerLord'] = self::GetServerLord();
		
		return $object;
	}
	
}