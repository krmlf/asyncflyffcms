<?php 

class Itemshop extends Database {
	
	private function LoadItemShopCategorys(){
		$cats = self::Select(array('id', 'category'), 'Website', 'ShopCategory', null, ' ORDER BY [id]');
		return $cats['Result'];
	}
	
	private function CountItemsInCategorys(){
		$cats = self::LoadItemShopCategorys();
		$count = array();
		for($i = 1;$i <= count($cats);$i++){
			$count[$i] = self::Select(array('itemid'), 'Website', 'ShopList', array('category' => $cats[$i]['category'], 'active' => 1), null, null, 0, array('=', '='));
			$count[$i] = $count[$i]['Rows'];
		}
		return $count;
	}
	
	public function GetItemShopCategorys(){
		$cats = self::LoadItemShopCategorys();
		$count = self::CountItemsInCategorys();
		$output = array($cats, $count);
		return $output;
	}
	
	private function GetCurrencyIcon($dbCurr){
		$currency = array('dCoins' => 'coin_gold', 'vCoins' => 'coin_silver');
		return $currency[$dbCurr];
	}
	
	private function GetCurrencyDesc($dbCurr){
		$currency = array('dCoins' => 'Donate Coins', 'vCoins' => 'Vote Coins');
		return $currency[$dbCurr];
	}
	
	public function SelectCharacterFromMultipleAccounts(){
		$getAccounts = self::Select(array('ig_accountname'), 'Website', 'WebAccount', array('id' => intval($_SESSION['id'])));
		if($getAccounts['Rows']){
			$getAccounts = explode(';', $getAccounts['Result'][1]['ig_accountname']);
			unset($getAccounts[count($getAccounts)-1]);
			$chars = array();
			$saveChars = array();
			$getChars = array();
			for($i = 0;$i <= count($getAccounts)-1;$i++){
				$chars[$i] = self::Select(array('m_szName'), 'Character', 'CHARACTER_TBL', array('account' => $getAccounts[$i], 'isblock' => 'F'), null, null, 0, array('=', '='));
				if($chars[$i]['Rows'] <= 0){
					unset($chars[$i]);
				}else {
					for($charCount = 1;$charCount <= count($chars[$i]['Result']);$charCount++){
						$saveChars[$i][$charCount] = $chars[$i]['Result'][$charCount]['m_szName'];
					}
					foreach($saveChars[$i] as $key => $val){
						$getChars[] = $val;
					}
				}
			}
			return $getChars;
		}
	}
	
	public function GetCategoryItemShopList($post){
		$post = Validation::ValidateInput($post);
		$list = self::Select(array('id', 'itemid', 'itemname', 'count', 'price', 'description', 'currency', 'szIcon', 'secure'), 'Website', 'ShopList', array('category' => $post['cat'], 'active' => 1), ' ORDER BY [id]', null, 0, array('=', '='));
		if($list['Rows'] > 0){
			$list['bool'] = true;
			for($i = 1;$i <= count($list['Result']);$i++){
				$list['Result'][$i]['currencyIcon'] = self::GetCurrencyIcon($list['Result'][$i]['currency']);
				$list['Result'][$i]['currencyDesc'] = self::GetCurrencyDesc($list['Result'][$i]['currency']);
			}
			return $list;
		}else {
			return array('bool' => false);
		}
	}
	
	public function ValidationBeforeSending($post){
		$post = Validation::ValidateInput($post);
		$requestItem = self::Select(array('id'), 'Website', 'ShopList', array('secure' => $post['secure']));
		if($requestItem['Rows'] > 0){
			$player = self::Select(array('m_idPlayer'), 'Character', 'CHARACTER_TBL', array('m_szName' => $post['player']));
			if($player['Rows'] > 0){
				$selectItem = self::Select(array('itemid', 'itemname','count', 'price', 'currency'), 'Website', 'ShopList', array('secure' => $post['secure']));
				$checkPlayerCash = self::Select(array($selectItem['Result'][1]['currency']), 'Website', 'WebAccount', array('username' => $_SESSION['user']));
				$checkPlayerCash = $checkPlayerCash['Result'][1][$selectItem['Result'][1]['currency']];
				if($selectItem['Result'][1]['price'] >= 0 and $checkPlayerCash >= 0){
					if($checkPlayerCash >= $selectItem['Result'][1]['price']){
						$setMoney = $checkPlayerCash - $selectItem['Result'][1]['price'];
						$object = array(
							'm_idPlayer' => $player['Result'][1]['m_idPlayer'],
							'serverindex' => '01',
							'Item_Name' => $selectItem['Result'][1]['itemid'],
							'Item_count' => intval($selectItem['Result'][1]['count']),
							'idSender' => '0000000',
						);
						$var = self::SendItemToPlayer($object);
						if($var){
							self::Update(array($selectItem['Result'][1]['currency'] => $setMoney), array('username' => $_SESSION['user']), 'Website', 'WebAccount');
							return Validation::Alert2('success', 'Item ' . $selectItem['Result'][1]['itemid'] . ' has been sent to your character.');
						}else {
							return Validation::Alert2('danger', 'There was an error while sending an item to your character.');
						}
					}else {
						return Validation::Alert2('danger', 'Your don\'t have enought ' . self::GetCurrencyDesc($selectItem['Result'][1]['currency']) . ' to buy this item.');
					}
				}else {
					return Validation::Alert2('danger', 'Cash and item price must be a positiv value.');
				}
			}else {
				return Validation::Alert2('danger', 'Player not found');
			}
		}else {
			return Validation::Alert2('danger', 'Item not found.');
		}
	}
	
	/**private function SendItemToPlayer($object){
		self::Insert($object, 'Character', 'ITEM_SEND_TBL');
	}*/
	
	private function SendItemToPlayer($object)
		{
			$socket = socket_create( AF_INET, SOCK_STREAM, SOL_TCP );
			$packet = pack( "VVVVVVVVV", 01, intval($object['m_idPlayer']), intval($object['m_idPlayer']), 101, intval($object['Item_Name']), intval($object['Item_count']), 0, SOCKET_CHECK1, SOCKET_CHECK2);
			if( @socket_connect( $socket, SOCKET_IP, SOCKET_PORT ) )
			{
				socket_write( $socket, $packet, strlen( $packet ) );
				socket_close( $socket );
				return true;
			}
			return false;
		}
	
}