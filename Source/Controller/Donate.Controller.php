<?php 

class Donate extends Database {
	
	public function SetPSCDonate($post){
		if(self::PreventSpam($post['g-recaptcha-response'])){
			if(Validation::InputCheck($post)){
				$post = Validation::ValidateInput($post);
				$code = $post['psc1'].$post['psc2'].$post['psc3'].$post['psc4'];
				self::Insert(array('code' => $code, 'value' => (int)$post['pscvalue'], 'date' => time(), 'account' => $_SESSION['user']), 'Website', 'DonatePSC');
				return array('success', 'Your donation has been saved. Please wait for a response.');
			}else {
				return array('danger', 'Please fill in all given fields.');
			}
		}else {
			return array('danger', 'Please confirm the captcha right below the input fields.');
		}
	}
	
	public function SetVoteCoins()
	{
		global $cfg;
		$vote = self::Select(array('vcoins', 'lastvote'), 'Website', 'WebAccount', array('username' => $_SESSION['user']));
		$last = $vote['Result'][1]['lastvote'];
		$coins = (int)$vote['Result'][1]['vcoins'];
		if($last == NULL || $last <= time()){
			$nextvote = time()+1*(int)$cfg['set_vote_pause']*60*60;
			$newcoins = $coins + (int)$cfg['set_vote_coins'];
			self::Update(array('vcoins' => $newcoins), array('username' => $_SESSION['user']), 'Website', 'WebAccount');
			self::Update(array('lastvote' => $nextvote), array('username' => $_SESSION['user']), 'Website', 'WebAccount');
			return array('success', 'Vote success. Please refresh your panel to see your vote coins.');
		}else {
			return array('danger', 'Can not vote now. New vote at: ' . date('d.m.Y h:i:s a'));
		}
	}
	
}