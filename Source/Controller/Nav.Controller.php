<?php

class Nav {
	
    public static function Link($param){
        $arr = array('active', null);
        if(isset($_GET[SITE_PARAM]) and !empty($_GET[SITE_PARAM])){
            if($param == $_GET[SITE_PARAM]){
                return $arr[0];
            }else {
                return $arr[1];
            }
        }else {
            if($param == DEFAULT_SITE){
                return $arr[0];
            }else {
                return $arr[1];
            }
        }
    }
	
}
