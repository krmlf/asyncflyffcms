<?php 

class Auth extends Database {
	
	
	/** WEBSITE AUTHENTICATION **/
	
	private function SelectAccountIDByUsername($user){
		$account = self::Select(array('id'), 'Website', 'WebAccount', array('username' => $user));
		return $account;
	}
	
	private function SelectEmailByUsername($email){
		$account = self::Select(array('id'), 'Website', 'WebAccount', array('email' => $email));
		return $account;
	}
	
	private function SelectPasswordByUsername($user){
		$password = self::Select(array('password'), 'Website', 'WebAccount', array('username' => $user));
		return $password['Result'][1]['password'];
	}
	
	private function SelectAuthorityByUsername($user){
		$auth = self::Select(array('authority'), 'Website', 'WebAccount', array('username' => $user));
		return $auth['Result'][1]['authority'];
	}
	
	private function SelectBanReasonByUsername($user){
		$auth = self::Select(array('banned'), 'Website', 'WebAccount', array('username' => $user));
		return $auth['Result'][1]['banned'];
	}
	
	public function Login($post){
		if(Validation::InputCheck($post)){
			$post = Validation::ValidateInput($post);
			$accountRows = self::SelectAccountIDByUsername($post['user']);
			if($accountRows['Rows'] > 0){
				$banned = self::SelectBanReasonByUsername($post['user']);
				if($banned === null){
					$password = self::SelectPasswordByUsername($post['user']);
					$hashedPassword = hash('sha256', $post['password']);
					if($password === $hashedPassword){
						session_start();
						$_SESSION = array();
						$_SESSION['id'] = $accountRows['Result'][1]['id'];
						$_SESSION['user'] = $post['user'];
						$_SESSION['authority'] = self::SelectAuthorityByUsername($post['user']);
						$_SESSION['securitykey'] = md5(hash('sha256', WEBSITE_SALT.rand(0, 1000).uniqid().$accountRows['Result'][1]['id'].microtime()));
						self::Update(array('lastlogin' => time()), array('username' => $post['user']), 'Website', 'WebAccount');
						Validation::DynamicRefresh(Validation::CreateUrl('News'));
					}else {
						return array('danger', 'Your password is wrong, please correct it.');
					}
				}else {
					return array('danger', '<b>The ban hammer has spoken.</b><br>'.$banned);
				}
			}else {
				return array('danger', 'This account does not exist.');
			}
		}else {
			return array('danger', 'Please fill in all given fields.');
		}
	}
	
	public function Registration($post){
		if(self::PreventSpam($post['g-recaptcha-response'])){
			if(Validation::InputCheck($post)){
				$post = Validation::ValidateInput($post);
				if(strlen($post['user']) >= 3 && strlen($post['user']) <= 20){
					$accountRows = self::SelectAccountIDByUsername($post['user']);
					if($accountRows['Rows'] <= 0){
						if($post['password'] === $post['password2']){
							if(strpos($post['mail'], '@')){
								if($post['mail'] === $post['mail2']){
									$email = self::SelectEmailByUsername($post['mail']);
									if($email['Rows'] <= 0){
										$post['password'] = hash('sha256', $post['password']);
										self::Insert(array('username' => $post['user'], 'password' => $post['password'], 'email' => $post['mail'], 'creationdate' => time()), 'Website', 'WebAccount');
										return array('success', 'You have successfull registered an website account.');
									}else {
										return array('danger', 'This email address is already used by another user.');
									}
								}else {
									return array('danger', 'Your email addresses does not match.');
								}
							}else {
								return array('danger', 'This is not a valide email address.');
							}
						}else {
							return array('danger', 'Your passwords does not match.');
						}
					}else {
						return array('danger', 'This username is already in use. Please choose another username.');
					}
				}else {
					return array('danger', 'Your username is too long. The required length is 3 chars and only 20 chars are allowed.');
				}
			}else {
				return array('danger', 'Please fill in all given fields.');
			}
		}else {
			return array('danger', 'Please confirm the captcha right below the input fields.');
		}
	}
	
	/** WEBSITE AUTHENTICATION **/
	
	private function SelectIngameAccountIDByUsername($user){
		$credentials = self::Select(array('account'), 'Account', 'ACCOUNT_TBL', array('account' => $user));
		return $credentials;
	}
	
	private function GetIngameAccountAmount($sessionUser){
		$amount = self::Select(array('ig_accountname'), 'Website', 'WebAccount', array('username' => $sessionUser));
		$string = $amount['Result'][1]['ig_accountname'];
		$amount = $amount['Result'][1]['ig_accountname'];
		if($amount !== null){
			$amount = explode(';', $amount);
			unset($amount[count($amount)-1]);
			$amount = count($amount);
		}else {
			$amount = 0;
		}
		
		return array($amount, $string);
	}
	
	private function SelectOldAccountByUsername($user){
		$acc = self::Select(array('account', 'dcoins', 'vcoins', 'password', 'active'), 'Website', 'OldAccounts', array('account' => $user));
		return $acc;
	}
	
	public function RecoverIngameAccount2($post, $ingameAccountLimit, $sessionUser)
	{
		if(self::PreventSpam($post['g-recaptcha-response'])){
			if(Validation::InputCheck($post)){
				$post = Validation::ValidateInput($post);
				$ingameAccountAmountResponse = self::GetIngameAccountAmount($sessionUser);
				if($ingameAccountAmountResponse[0] < $ingameAccountLimit){
					if(strlen($post['user']) >= 3 && strlen($post['user']) <= 16){
						$acc = self::Select(array('account', 'password'), 'Account', 'ACCOUNT_TBL', array('account' => $post['user']));
						if($acc['Rows'] > 0){
							$acc = $acc['Result'][1];
							$logged = self::Select(array('account'), 'Website', 'LOG_RecoveredAccounts', array('account' => $post['user']));
							if($logged['Rows'] <= 0){
								$cryptPw = md5(SERVER_SALT.$post['password']);
								if($cryptPw == $acc['password']){
									$newAccountList = $ingameAccountAmountResponse[1].$acc['account'].';';
									self::Update(array('ig_accountname' => $newAccountList), array('username' => $sessionUser), 'Website', 'WebAccount');
									self::Insert(array('account' => $acc['account'], 'logged' => time()), 'Website', 'LOG_RecoveredAccounts');
									return array('success', 'Your old account has been recovered and added to your account list.');
									test::pre($newAccountList);
									test::pre($acc);
								}else {
									return array('danger', 'You have entered a wrong password.');
								}
							}else {
								return array('danger', 'This account has already been recovered.');
							}
						}else {
							return array('danger', 'This account does not exist.');
						}
					}else {
						return array('danger', 'Your username is too long. The required length is 3 chars and only 12 chars are allowed.');
					}
				}else {
					return array('danger', 'You reached your ingame account limit. To create a new account please deactivate one of your accounts.');
				}
			}else {
				return array('danger', 'Please fill in all given fields.');
			}
		}else {
			return array('danger', 'Please confirm the captcha right below the input fields.');
		}
	}
	
	/**public function RecoverIngameAccount($post, $ingameAccountLimit, $sessionUser){
		if(self::PreventSpam($post['g-recaptcha-response'])){
			if(Validation::InputCheck($post)){
				$post = Validation::ValidateInput($post);
				$ingameAccountAmountResponse = self::GetIngameAccountAmount($sessionUser);
				if($ingameAccountAmountResponse[0] < $ingameAccountLimit){
					if(strlen($post['user']) >= 3 && strlen($post['user']) <= 12){
						$oldaccount = self::SelectOldAccountByUsername($post['user']);
						if($oldaccount > 0){
							$oldaccount = $oldaccount['Result'][1];
							if(md5(SERVER_SALT.$post['password']) == $oldaccount['password']){
								if($oldaccount['active'] == 0){
									$newAccountList = $ingameAccountAmountResponse[1].$oldaccount['account'].';';	
									$oldCoinAmount = self::Select(array('dcoins', 'vcoins'), 'Website', 'WebAccount', array('username' => $sessionUser));
									$oldCoinAmount = $oldCoinAmount['Result'][1];
									self::Update(array('ig_accountname' => $newAccountList), array('username' => $sessionUser), 'Website', 'WebAccount');
									self::Update(array('vcoins' => $oldaccount['vcoins'] + $oldCoinAmount['vcoins']), array('username' => $sessionUser), 'Website', 'WebAccount');
									self::Update(array('dcoins' => $oldaccount['dcoins'] + $oldCoinAmount['dcoins']), array('username' => $sessionUser), 'Website', 'WebAccount');
									self::Update(array('active' => 1), array('account' => $oldaccount['account']), 'Website', 'OldAccounts');
									return array('success', 'Your account has been added to your account list. Your coins has been saved, too.');
								}else {
									return array('danger', 'This account was already activated.');
								}
							}else {
								return array('danger', 'Your entered a wrong password. Please try again.');
							}
						}else {
							return array('danger', 'This account does not exist.');
						}
						test::pre($oldaccount);
					}else {
						return array('danger', 'Your username is too long. The required length is 3 chars and only 12 chars are allowed.');
					}
				}else {
					return array('danger', 'You reached your ingame account limit. To create a new account please deactivate one of your accounts.');
				}
			}else {
				return array('danger', 'Please fill in all given fields.');
			}
		}else {
			return array('danger', 'Please confirm the captcha right below the input fields.');
		}
	}*/
	
	public function RegisterIngameAccount($post, $ingameAccountLimit, $sessionUser){
		if(self::PreventSpam($post['g-recaptcha-response'])){
			if(Validation::InputCheck($post)){
				$post = Validation::ValidateInput($post);
				$ingameAccountAmountResponse = self::GetIngameAccountAmount($sessionUser);
				if($ingameAccountAmountResponse[0] < $ingameAccountLimit){
					if(strlen($post['user']) >= 3 && strlen($post['user']) <= 12){
						$accountID = self::SelectIngameAccountIDByUsername($post['user']);
						if($accountID['Rows'] <= 0){
							if($post['password'] === $post['password2']){
								$cryptPw = md5(SERVER_SALT.$post['password']);
								self::Insert(array('account' => $post['user'], 'password' => $cryptPw, 'isuse' => 'F', 'member' => 'A', 'realname' => 'F', 'cash' => 0), 'Account', 'ACCOUNT_TBL');
								self::Insert(array('account' => $post['user'], 'gamecode' => 'A000', 'tester' => '2', 'm_chLoginAuthority' => 'F', 'regdate' => '0x'.bin2hex(date('Ymd H:i:s')), 'isuse' => 'J'), 'Account', 'ACCOUNT_TBL_DETAIL');
								$newAccountList = $ingameAccountAmountResponse[1].$post['user'].';';	
								self::Update(array('ig_accountname' => $newAccountList), array('username' => $sessionUser), 'Website', 'WebAccount');
								return array('success', 'Your ingame account was successfully created.');
							}else {
								return array('danger', 'Your passwords does not match.');
							}
						}else {
							return array('danger', 'This username is already in use. Please choose another username.');
						}
					}else {
						return array('danger', 'Your username is too long. The required length is 3 chars and only 12 chars are allowed.');
					}
				}else {
					return array('danger', 'You reached your ingame account limit. To create a new account please deactivate one of your accounts.');
				}
			}else {
				return array('danger', 'Please fill in all given fields.');
			}
		}else {
			return array('danger', 'Please confirm the captcha right below the input fields.');
		}
	}
	
	//PANEL AUTHENTICATION
	
	public function GetPanelAccess($post, $sessionKey = 'id'){
		if(isset($_SESSION[$sessionKey])){
			if(!isset($_SESSION['trys'])){$_SESSION['trys'] = 0;}
			$getUserTrys = self::Select(array('trys'), 'Website', 'WebAccount', array('username' => $_SESSION['user']));
			$getUserTrys = $getUserTrys['Result'][1]['trys'];
            if($_SESSION['trys'] <= 3){
				if($_SESSION['authority'] === 'Z' or $_SESSION['authority'] === 'P' or $_SESSION['authority'] === 'O'){
					if(!isset($_SESSION['panelkey'])){
						$getKey = self::Select(array('password'), 'Website', 'Securitykey', array('authority' => $_SESSION['authority']));
						if($getKey['Rows'] > 0){
							$getKey = $getKey['Result'][1]['password'];
							if($getKey === $post['accessPanelKey']){
								if($getUserTrys <= 3){
									$_SESSION['panelkey'] = $getKey;
									self::Update(array('trys' => 0), array('username' => $_SESSION['user']), 'Website', 'WebAccount');
									return Validation::Alert2('success', 'Access granted.');
								}else {
									return Validation::Alert2('danger', 'Too many trys.');
								}
							}else {
								self::Update(array('trys' => $getUserTrys+1), array('username' => $_SESSION['user']), 'Website', 'WebAccount');
								$_SESSION['trys'] = $_SESSION['trys'] + 1;
								return Validation::Alert2('danger', 'Wrong key.');
							}
						}else {
							self::Update(array('trys' => $getUserTrys+1), array('username' => $_SESSION['user']), 'Website', 'WebAccount');
							$_SESSION['trys'] = $_SESSION['trys'] + 1;
							return Validation::Alert2('danger', 'No key granted.');
						}
					}else {
						return Validation::Alert2('danger', 'You alread have a key.');
					}
				}else {
					self::Update(array('trys' => $getUserTrys+1), array('username' => $_SESSION['user']), 'Website', 'WebAccount');
					$_SESSION['trys'] = $_SESSION['trys'] + 1;
					return Validation::Alert2('danger', 'Your aren\'t a staff member.');
				}
			}else {
				self::Update(array('trys' => $getUserTrys+1), array('username' => $_SESSION['user']), 'Website', 'WebAccount');
				return Validation::Alert2('danger', 'Too many trys.');
			}
        }else {
            return Validation::Alert2('danger', 'You have to login.');
        }
	}
	
}