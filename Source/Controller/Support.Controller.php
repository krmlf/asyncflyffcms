<?php 

/**
* Project: Ticketsystem / Supportsystem
* Base: MSSQL (Fly For Fun)
* Author: AMAZEN
* Querys: Custom Database Query Mapper (replaceable with normal querys or something else)
**/

class Support extends Database {
	
	// User Side Functions
	
	private function DefineTicketState($state){
		$array = array(
			0		=>		'Open',
			1		=>		'In process',
			2		=>		'Closed',
		);
		return $array[$state];
	}
	
	public function GetUserTicketList($session){
		$fishing = self::Select(array('id', 'author', 'date', 'moderator', 'state', 'title'), 'Website', 'SupportTicket', array('author' => $session['user']), ' ORDER BY [id] DESC');
		if($fishing['Rows'] > 0){ 
			for($i = 1;$i <= $fishing['Rows'];$i++){
				$fishing['Result'][$i]['state'] = self::DefineTicketState($fishing['Result'][$i]['state']);
				$fishing['Result'][$i]['comments'] = self::GetCommentCount($fishing['Result'][$i]['id']);
			}
		}else {
			$fishing = null;
		}
		return $fishing;
	}
	
	//Insert new tickets
	
	public function InsertNewTickets($post, $session){
		if(self::PreventSpam($post['g-recaptcha-response'])){
			if(Validation::InputCheck($post)){
				$post = Validation::ValidateInput($post);
				self::Insert(array('author' => $session, 'date' => time(), 'title' => $post['title'], 'text' => $post['text']), 'Website', 'SupportTicket');
				return array('success', 'Your ticket hass been saved.');
			}else {
				return array('danger', 'Please fill in all given fields.');
			}
		}else {
			return array('danger', 'Please confirm the captcha right below the input fields.');
		}
	}
	
	//Ticket Panel
	
	private function GetCommentCount($ticketID){
		$count = self::Select(array('id'), 'Website', 'SupportComment', array('ticketid' => intval($ticketID)));
		return $count['Rows'];
	}
	
	private function SelectUserTicketContent($ticketID){
		$fishing = self::Select(array('id', 'author', 'date', 'moderator', 'state', 'title', 'text'), 'Website', 'SupportTicket', array('id' => intval($ticketID)));
		if($fishing['Rows'] > 0){ 
			$fishing['Result'][1]['state'] = self::DefineTicketState($fishing['Result'][1]['state']);
			$fishing['ticket'] = $fishing['Result'][1];
			$fishing['ticket']['comments'] = self::GetCommentCount($ticketID);
		}else {
			$fishing = null;
		}
		return $fishing;
	}
	
	private function ValidateUserTicket($session, $ticketID){
		$fishing = self::Select(array('id'), 'Website', 'SupportTicket', array('id' => intval($ticketID), 'author' => $session['user']), null, null, 0, array('=', '='));
		if($fishing['Rows'] > 0){
			$bool[] = $fishing['Result'][1]['id'];
		}else {
			$bool = null;
		}
		return $bool;
	}
	
	//Coments
	
	public function InsertComment($post, $session){
		if(self::PreventSpam($post['g-recaptcha-response'])){
			if(Validation::InputCheck($post)){
				$post = Validation::ValidateInput($post);
				self::Insert(array('ticketid' => intval($post['id']), 'author' => $session['user'], 'comment' => $post['commenttext'], 'date' => time(), 'authority' => $session['authority']), 'Website', 'SupportComment');
				return array('success', 'Comment was successfully saved.');
			}else {
				return array('danger', 'Please fill in all given fields.');
			}
		}else {
			return array('danger', 'Please confirm the captcha right below the input fields.');
		}
	}
	
	public function GetUserCommentContent($session, $ticketID){
		$fishing = self::Select(array('id', 'author', 'comment', 'date', 'authority'), 'Website', 'SupportComment', array('ticketid' => intval($ticketID)), ' ORDER BY [id] DESC');
		if($fishing['Rows'] > 0){
			$fishing['bool'] = true;
			return $fishing;
		}else {
			return array('warning', 'Actually there are no responses.', 'bool' => false);
		}
	}
	
	public function GetUserTicketContent($session, $ticketID){
		$isUsersTicket = self::ValidateUserTicket($session, $ticketID);
		if($isUsersTicket !== null){
			$userTickets = self::SelectUserTicketContent($ticketID);
			$userTickets['bool'] = true;
			return $userTickets;
		}else {
			return array('danger', 'The choosen ticket does not exist or is given to another user.', 'bool' => false);
		}
	}
	
}