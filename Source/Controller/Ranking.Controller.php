<?php 

class Ranking extends Database {
	/* Player Ranking Functions */
	public static function GetClassIcon($classID) {
		$class = Flyff::GetClassName($classID);
		if(!$class[1]){
			$class = $class[0];
		}else {
			$class = $class[0] . ' ' . $class[1];;
		}
		return $class;
	}
	
	public static function GetClassName($classID) {
		$class = Flyff::GetClassName($classID);
		$class = $class[0];
		return $class;
	}
	
	public function GetPlayerList($limit) {
		$get = self::Select(array('m_szName', 'm_nJob', 'm_nLevel', 'TotalPlayTime'), 'Character', 'CHARACTER_TBL', array('m_chAuthority' => 'F', 'isblock' => 'F'), ' ORDER BY [m_nLevel] DESC, [TotalPlayTime] DESC', 'TOP '.$limit.' ', 0 , array('=', '='));
		if($get['Rows'] > 0){
			$get['bool'] = true;
			return $get;
		}else {
			$get['bool'] = false;
			return array('danger', 'There are no ranked players yet.');
		}
	}
	
	public function SelectGuildIDByPlayer($player){
		$guildID = self::Select(array('m_idGuild'), 'Character', 'GUILD_MEMBER_TBL', array('m_idPlayer' => $player), null);
		if($guildID['Rows'] > 0){
			$guildID = $guildID['Result'][1]['m_idGuild'];
		}else {
			$guildID = 'Guildless';
		}
		return $guildID;
	}
	
	private function SelectGuildNameByGuildID($guildID){
		$guildName = $guildID;
		if($guildName !== 'Guildless'){
			$guildName = self::Select(array('m_szGuild'), 'Character', 'GUILD_TBL', array('m_idGuild' => $guildID), null);
			$guildName = $guildName['Result'][1]['m_szGuild'];
		}
		return $guildName;
	}
	
	private function SetMultiserverView($multiserver){
		if($multiserver === '1'){
			return 'Online';
		}else {
			return 'Offline';
		}
	}
	
	public function GetPlayerRanking($limit){
		if(gettype($limit) === 'integer'){
			$player = self::Select(array('m_idPlayer', 'm_szName', 'm_nJob', 'm_nLevel', 'TotalPlayTime', 'Multiserver'), 'Character', 'CHARACTER_TBL', array('m_chAuthority' => 'F', 'isblock' => 'F'), ' ORDER BY [m_nLevel] DESC, [TotalPlayTime] DESC', 'TOP '.$limit.' ', 0, array('=', '='));
			$rows = $player['Rows'];
			if($rows > 0){
				$player = $player['Result'];
				for($i = 1;$i <= $rows;$i++){
					$player[$i]['m_szGuild'] = self::SelectGuildNameByGuildID(self::SelectGuildIDByPlayer($player[$i]['m_idPlayer']));
					$player[$i]['Multiserver'] = self::SetMultiserverView($player[$i]['Multiserver'] );
				}
				$player['bool'] = true;
				return $player;
			}else {
				return array('danger', 'Actually we have no registered players.', 'bool' => false, );
			}
		}else {
			return array('danger', 'Please stop forging the values.', 'bool' => false, );
		}
	}
	
	/* Guild Ranking Functions */
	
	private function CheckGuildMemberAuthority($guildID){
		$response = 1;
		$playerID = self::Select(array('m_idPlayer'), 'Character', 'GUILD_MEMBER_TBL', array('m_idGuild' => $guildID));
		$authority = array();
		for($i = 1;$i <= $playerID['Rows'];$i++){
			$authority[$i] = self::Select(array('m_chAuthority'), 'Character', 'CHARACTER_TBL', array('m_idPlayer' => $playerID['Result'][$i]['m_idPlayer']));
			if($authority[$i]['Result'][1]['m_chAuthority'] !== 'F'){
				$response = 0;
			}
		}
		return $response;
	}
	
	private function SelectLeaderNameByGuildID($leaderID){
		$leaderName = self::Select(array('m_szName'), 'Character', 'CHARACTER_TBL', array('m_idPlayer' => $leaderID), null, 'TOP 1 ');
		return $leaderName['Result'][1]['m_szName'];
	}
	
	private function BetterGuildCreateFormat($creationDate, $monthList){
		$creationDate = substr($creationDate, 0, strlen($creationDate)-12);
		$creationDate = explode('-', $creationDate);
		$creationDate[1] = $monthList[$creationDate[1]-1];
		$creationDate = $creationDate[1] . ', ' . $creationDate[0];
		return $creationDate;
	}
	private function GetGuildMemberAmount($guildID){
		$amount = self::Select(array('m_idPlayer'), 'Character', 'GUILD_MEMBER_TBL', array('m_idGuild' => $guildID));
		return $amount['Rows'];
	}
	
	private function GetGuildLeader($guildID){
		$leader = self::Select(array('m_idPlayer', 'm_nMemberLv'), 'Character', 'GUILD_MEMBER_TBL', array('m_idGuild' => $guildID), ' ORDER by [m_nMemberLv]', 'TOP 1 ');
		$leader = self::SelectLeaderNameByGuildID($leader['Result'][1]['m_idPlayer']);
		return $leader;
	}
	//in relation to slot amount + farbcodes
	private function SplitSlotValues($slots, $amount){
		$unit = $slots / 4;
		
		if($amount <= $unit){
			return '50d3a9';
		}
		elseif($amount <= ($unit * 2)){
			return '7bd350';
		}
		elseif($amount <= ($unit * 3)){
			return 'd3b050';
		}
		elseif($amount <= ($unit * 4)){
			return 'd35050';
		}
		
	}
	
	public function GetGuildRanking($limit, $monthList, $slots){
		if(gettype($limit) === 'integer'){
			$guild = self::Select(array('m_idGuild', 'm_szGuild', 'm_nLevel', 'CreateTime'), 'Character', 'GUILD_TBL', null, ' ORDER BY [m_nLevel] DESC, [CreateTime] DESC', 'TOP '.$limit. ' ');
			$rows = $guild['Rows'];
			if($rows > 0){
				$guild = $guild['Result'];
				$a = 0;
				for($i = 1;$i <= $rows;$i++){
					$guild[$i]['Authority'] = self::CheckGuildMemberAuthority($guild[$i]['m_idGuild']);
					if($guild[$i]['Authority'] === 1){
						$guild[$i]['MemberAmount'] = self::GetGuildMemberAmount($guild[$i]['m_idGuild']);
						$guild[$i]['CreateTime'] = self::BetterGuildCreateFormat($guild[$i]['CreateTime'],$monthList);
						$guild[$i]['GuildLeader'] = self::GetGuildLeader($guild[$i]['m_idGuild']);
						$guild[$i]['SlotSplitter'] = self::SplitSlotValues($slots, $guild[$i]['MemberAmount']);
						$a++;
					}else {
						unset($guild[$i]);
					}
				}
				$mirror = array();
				foreach($guild as $key => $value){
					$mirror[] = $guild[$key];
				}
				$mirror['bool'] = true;
				$mirror['hide'] = $a;
				return $mirror;
			}else {
				return array('bool' => false, 'danger', 'There are actually none registered guilds.');
			}
		}
	}
	
}