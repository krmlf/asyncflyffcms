<?php 

class UserInformation extends Database {
	
	private $session;
	public $pointer = array();
	
	//USERVIEW
	
	private function GetCharacterInformations(){
		if(isset($this->pointer['chractercredentials'])){
			for($accounts = 0;$accounts <= count($this->pointer['chractercredentials'])-1;$accounts++){
				if(isset($this->pointer['chractercredentials'][$accounts])){
					for($slots = 1;$slots <= count($this->pointer['chractercredentials'][$accounts]);$slots++){
						$fishing = self::Select(array('m_nJob', 'm_nLevel', 'TotalPlayTime', 'Multiserver'), 'Character', 'CHARACTER_TBL', array('m_idPlayer' => $this->pointer['chractercredentials'][$accounts][$slots]['id']));
						$this->pointer['chractercredentials'][$accounts][$slots]['info'] = array(
							'class'		=>		array(
										'name'		=>		Ranking::GetClassName($fishing['Result'][1]['m_nJob']),
										'icon'		=>		$fishing['Result'][1]['m_nJob'],
							),
							'level'		=>		$fishing['Result'][1]['m_nLevel'],
							'playtime'		=>		$fishing['Result'][1]['TotalPlayTime'],
							'connection'		=>		$fishing['Result'][1]['Multiserver'],
						);
					}
				}
			}
			ksort($this->pointer['chractercredentials']);
		}
	}
	
	private function GetUserIngameAccounts(){
		$this->pointer['ingameaccounts'] = null;
		$fishing = self::Select(array('ig_accountname'), 'Website', 'WebAccount', array('username' => $this->session['user']));
		if($fishing['Rows'] > 0){
			$fishing = explode(';', $fishing['Result'][1]['ig_accountname']);
			unset($fishing[count($fishing)-1]);
			
			for($i = 0;$i <= count($fishing)-1;$i++){
				$this->pointer['ingameaccounts'][$i] = $fishing[$i];
			}
		}
	}
	
	private function GetAccountCreationDate(){
		for($i = 0;$i <= count($this->pointer['ingameaccounts'])-1;$i++){
			$fishing = self::Select(array('regdate'), 'Account', 'ACCOUNT_TBL_DETAIL', array('account' => $this->pointer['ingameaccounts'][$i]));
			$this->pointer['CreationDate'][$i] = $fishing['Result'][1]['regdate'];
		}
		
	}
	
	private function GetNonDeletedCharacterCredentials(){
		$fishing = array();
		for($i = 0;$i <= count($this->pointer['ingameaccounts'])-1;$i++){
			$fishing[$i] = self::Select(array('m_idPlayer', 'm_szName', 'playerslot'), 'Character', 'CHARACTER_TBL', array('account' => $this->pointer['ingameaccounts'][$i], 'isblock' => 'F'), ' ORDER BY [playerslot]', 'TOP 3', 0, array('=', '='));
			if($fishing[$i]['Rows'] > 0){
				for($c = 1;$c <= $fishing[$i]['Rows'];$c++){
					$this->pointer['chractercredentials'][$i][$c] = array(
						'id'		=>		$fishing[$i]['Result'][$c]['m_idPlayer'],
						'name'		=>		$fishing[$i]['Result'][$c]['m_szName'],
						'slot'		=>		$fishing[$i]['Result'][$c]['playerslot'],
					);
				}
			}
		}
	}
	
	private function GetCoinAmount(){
		$fishing = array(
			'donate'	=>		self::Select(array('dcoins'), 'Website', 'WebAccount', array('username' => $this->session['user'])),
			'vote'		=>		self::Select(array('vcoins'), 'Website', 'WebAccount', array('username' => $this->session['user'])),
		);
		$this->pointer['coins'] = array(
			'donate'	=>		$fishing['donate']['Result'][1]['dcoins'],
			'vote'		=>		$fishing['vote']['Result'][1]['vcoins'],
		);
	}
	
	public function GetUserCredentialInformation(){
		
		//define user session
		$this->session = $_SESSION;
		
		//get donate and vote coin amount
		self::GetCoinAmount();
		
		//get user ingame accounts
		self::GetUserIngameAccounts();
		
		//get user ingame accounts
		self::GetAccountCreationDate();
		
		//Get character credentials by accountid (ingame)
		self::GetNonDeletedCharacterCredentials();
		
		//Get minfied character informations
		self::GetCharacterInformations();
		
		return $this->pointer;
	}
	
}