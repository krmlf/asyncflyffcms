<?php 

class Download extends Database {
	
	
	
	public function GetClientDownloads(){
		$values = self::Select(array('id', 'description', 'mirror', 'size', 'registered', 'host'), 'Website', 'Download', array('active' => 1), ' ORDER BY [id] DESC');
		
		if($values['Rows'] > 0){
			$values['bool'] = true;
			return $values;
		}else {
			return array('danger', 'There are actually no downloads available.', 'bool' => false);
		}
	}
	
}