<?php 

$siteLocation = $path . DEFAULT_TEMPLATE . '/Pages/';
$fileType = '/Overview.php';

if(isset($_GET[SITE_PARAM]) && !empty($_GET[SITE_PARAM])) {
 $_GET[SITE_PARAM] = preg_replace('/[^A-Za-z]/', '', $_GET[SITE_PARAM]);
 if(file_exists($siteLocation . $_GET[SITE_PARAM] . $fileType)) {
  require_once($siteLocation . $_GET[SITE_PARAM] . $fileType);
 } 
 else {
  require_once($siteLocation.DEFAULT_SITE.$fileType);
 }
}else {
  require_once($siteLocation.DEFAULT_SITE.$fileType);
 }
 
