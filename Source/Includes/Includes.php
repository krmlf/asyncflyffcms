<?php 

require_once($path.'Source/Includes/Constants.php');

require_once($path.'Source/Includes/Connection.php');

require_once($path.'Source/Classes/Test.php');

require_once($path.'Source/Classes/Database.php');
require_once($path.'Source/Classes/Config.php');
require_once($path.'Source/Includes/Settings.php');
require_once($path.'Source/Classes/Translation.php');
require_once($path.'Source/Classes/Statistics.php');
require_once($path.'Source/Classes/Keygen.php');
require_once($path.'Source/Classes/Flyff.php');
require_once($path.'Source/Classes/Giftbox.php');

require_once($path.'Source/Classes/Common.php');

require_once($path.'Source/Controller/Nav.Controller.php');
require_once($path.'Source/Controller/Key.Controller.php');
require_once($path.'Source/Controller/Ranking.Controller.php');
require_once($path.'Source/Controller/News.Controller.php');
require_once($path.'Source/Controller/Ranking.Controller.php');
require_once($path.'Source/Controller/Stats.Controller.php');
require_once($path.'Source/Controller/Download.Controller.php');
require_once($path.'Source/Controller/Auth.Controller.php');
require_once($path.'Source/Controller/User.Controller.php');
require_once($path.'Source/Controller/Support.Controller.php');
require_once($path.'Source/Controller/Itemshop.Controller.php');
require_once($path.'Source/Controller/Donate.Controller.php');

