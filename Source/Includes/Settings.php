<?php 

$config = new Config();
$cfg = $config->GetConfig();

$cfg['alertkeys'] = array('success', 'warning', 'info', 'danger');

$cfg['MonthList'] = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
