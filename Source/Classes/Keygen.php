<?php 

class Keygen {
	
	private static function Generate($salt){
		$key = md5(uniqid() . $salt . time() . date('D.m.Y H:i:s'));
		return $key;
	}
	
	public static function Split($salt, $arr = array()){
		$key = self::Generate($salt);
		
		$arr[0] = substr($key, 0, 8);
		$arr[1] = substr($key, 8, 8);
		$arr[2] = substr($key, 16, 8);
		$arr[3] = substr($key, 24, 8);
		
		return $arr;
	}
	
	public static function Craft($salt){
		$key = self::Split($salt);
		
		return $key[0] . '-' . $key[1] . '-' . $key[2] . '-' . $key[3];
	}
	
}