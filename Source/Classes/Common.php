<?php


class Validation {
	
	public static function Access($sessionKey = 'id'){
        if(isset($_SESSION[$sessionKey])){
            return 1;
        }else {
            return 0;
        }
	}
    
    public static function DynamicRefresh($site, $bool = true){
        if($bool){echo '<script type="text/javascript">window.location = "' . $site . '";</script>';}
    }
    
    public static function InputCheck($array){
        $result = true;
        foreach($array as $key => $value){
            if(empty($array[$key])){
                $result = false;
                break;
            }
        }
        return $result;
    }
    
    public static function ValidateInput($array){
        foreach($array as $key => $value){
            $array[$key] = htmlspecialchars($value);
            $array[$key] = htmlentities($value);
            $array[$key] = strip_tags($value);
        }
        return $array;
    }
    
    public static function Alert($key, $value){
        $alertkey = $key;
        $alertvalue = $value;
        $rq = require_once(TEMPLATE_PATH . DEFAULT_TEMPLATE . '/Construct/Alert.php');
    }
    
    public static function Alert2($key, $value){
        $out = '<div class="alert alert-' . $key . '">
                    ' . nl2br(utf8_encode($value)) . '
                </div>';
        return $out;
    }
    
    public static function Logout(){
        if(isset($_GET['logout'])){
            session_destroy();
            echo '<script type="text/javascript">window.location = "' . self::CreateUrl(DEFAULT_SITE) . '";</script>';
        }
    }
    
    public static function GetCookie($param){
        if(isset($_COOKIE[$param])){
            if(!empty($_COOKIE[$param])){
                return $_COOKIE[$param];
            }
        }
    }
    
    public static function CreateUrl($value){
        return '?p='.$value;
    }
    
}
