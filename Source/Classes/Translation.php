<?php 

class Translation extends Database {
	
	private function CompareString($value){
		$lang = self::GetLanguage();
		//Get the line of each translation where value equals the database key
		$raw = self::Select(array('key', $lang), 'translation', array('key' => $value), ' LIMIT 1');
		//Check if the database key is already in our database
		if($raw['Rows'] > 0){
			//If the key is already in the database check if the language row is not empty
			if($raw['Result'][0][$lang] == null){
				//If the language row is empty return the key value
				return $value;
				//If the language row is not empty, return the language value from our database
			}else {
				return $raw['Result'][0][$lang];
			}
		//If the database key does not exist it will be inserted in our database and then the script returns the key value
		}else {
			self::Insert(array('key' => $value), 'translation');
			return $value;
		}
	}
	
	private function GetLanguage(){
		//DEFAULT_LANG definieren
		$lang = DEFAULT_LANG;
		if(isset($_COOKIE['lang'])){
			if(!empty($_COOKIE['lang'])){
				$lang = $_COOKIE['lang'];
			}
		}
		return $lang;
	}
	
	public function Translate($value){
		$string = self::CompareString($value);
		return $string;
	}
	
}