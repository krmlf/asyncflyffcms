$(document).ready(function() {
    
    $(document).ready(function(e) {
        $.ajax({
            url         : "Templates/Erendora/Pages/News/Ajax/Ajax.Select.News.php", 
            beforeSend: function() {
                // setting a timeout
                $('#getNews').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#getNews').html(data).hide();
              $("#getNews").fadeIn(250);
        });
        
    });
    
    $('#newsTab').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            url         : "Templates/Erendora/Pages/News/Ajax/Ajax.Select.News.php", 
            beforeSend: function() {
                // setting a timeout
                $('#getNews').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#getNews').html(data).hide();
              $("#getNews").fadeIn(250);
        });
        
    });
    
    $('#updatesTab').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            url         : "Templates/Erendora/Pages/News/Ajax/Ajax.Select.Updates.php", 
            beforeSend: function() {
                // setting a timeout
                $('#getNews').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#getNews').html(data).hide();
              $("#getNews").fadeIn(250);
        });
    });
    
    $('#eventsTab').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            url         : "Templates/Erendora/Pages/News/Ajax/Ajax.Select.Events.php",  
            beforeSend: function() {
                // setting a timeout
                $('#getNews').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#getNews').html(data).hide();
              $("#getNews").fadeIn(250);
        });
    });
    
    $('#refreshSmallUserList').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            url         : "Templates/Erendora/Parts/Smallranking/Ajax/Ajax.Smallranking.php", 
            beforeSend: function() {
                // setting a timeout
                $('#getSmallUserList').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#getSmallUserList').html(data).hide();
              $("#getSmallUserList").fadeIn(250);
        });
    });
    
     $(document).ready(function(e) {
        $.ajax({
            url         : "Templates/Erendora/Parts/Smallranking/Ajax/Ajax.Smallranking.php", 
            beforeSend: function() {
                // setting a timeout
                $('#getSmallUserList').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#getSmallUserList').html(data).hide();
              $("#getSmallUserList").fadeIn(250);
        });
        
    });
    
    $(document).ready(function(e) {
        $.ajax({
            url         : "Templates/Erendora/Parts/ServerStats/Ajax/Ajax.Serverstats.php", 
            beforeSend: function() {
                // setting a timeout
                $('#loadSmallStats').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#loadSmallStats').html(data).hide();
              $("#loadSmallStats").fadeIn(250);
        });
        
    });
    
    $('#refreshSmallStats').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            url         : "Templates/Erendora/Parts/ServerStats/Ajax/Ajax.Serverstats.php", 
            beforeSend: function() {
                // setting a timeout
                $('#loadSmallStats').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#loadSmallStats').html(data).hide();
              $("#loadSmallStats").fadeIn(250);
        });
    });
    
    $(document).ready(function(e) {
        $.ajax({
            url         : "Templates/Erendora/Pages/Download/Ajax/Ajax.Downloads.php", 
            beforeSend: function() {
                // setting a timeout
                $('#listDownloads').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#listDownloads').html(data).hide();
              $("#listDownloads").fadeIn(250);
        });
        
    });
    
    $(document).ready(function(e) {
        $.ajax({
            url         : "Templates/Erendora/Pages/Playerranking/Ajax/Ajax.Playerranking.php",  
            beforeSend: function() {
                // setting a timeout
                $('#listplayerdefault').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#listplayerdefault').html(data).hide();
              $("#listplayerdefault").fadeIn(250);
        });
        
    });
    
    $('#loadPlayerRanking').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            url         : "Templates/Erendora/Pages/Playerranking/Ajax/Ajax.Playerranking.php", 
            beforeSend: function() {
                // setting a timeout
                $('#listplayerdefault').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#listplayerdefault').html(data).hide();
              $("#listplayerdefault").fadeIn(250);
        });
    });
    
    $(document).ready(function(e) {
        $.ajax({
            url         : "Templates/Erendora/Pages/Guildranking/Ajax/Ajax.Guildranking.php", 
            beforeSend: function() {
                // setting a timeout
                $('#listguilddefault').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#listguilddefault').html(data).hide();
              $("#listguilddefault").fadeIn(250);
        });
        
    });
    
    $('#loadGuildRanking').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            url         : "Templates/Erendora/Pages/Guildranking/Ajax/Ajax.Guildranking.php", 
            beforeSend: function() {
                // setting a timeout
                $('#listguilddefault').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#listguilddefault').html(data).hide();
              $("#listguilddefault").fadeIn(250);
        });
    });
    
    $('#setRegistration').on('click', function(e) {
        e.preventDefault();
        var formData = {
            'user'  : $('#user').val(),
            'password'  : $('#password').val(),
            'password2'  : $('#password2').val(),
            'mail'  : $('#mail').val(),
            'mail2'  : $('#mail2').val(),
            'g-recaptcha-response'  : $('#g-recaptcha-response').val(),
        };
        $.ajax({
            type        : "POST",
            url         : "Templates/Erendora/Pages/Signup/Ajax/Ajax.Signup.php", 
            data        : formData,
            beforeSend: function() {
                // setting a timeout
                $('#loadRegistration').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#loadRegistration').html(data).hide();
              $("#loadRegistration").fadeIn(250);
        });
    });
    
    $('#setLogin').on('click', function(e) {
        e.preventDefault();
        var formData = {
            'user'  : $('#user').val(),
            'password'  : $('#password').val(),
        };
        $.ajax({
            type        : "POST",
            url         : "Templates/Erendora/Pages/Login/Ajax/Ajax.Login.php", 
            data        : formData,
            beforeSend: function() {
                // setting a timeout
                $('#loadLogin').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#loadLogin').html(data).hide();
              $("#loadLogin").fadeIn(250);
        });
    });
    
    $('#refreshUserView').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            url         : "Templates/Erendora/Parts/SidePanel/Ajax/Ajax.Sidepanel.php", 
            beforeSend: function() {
                // setting a timeout
                $('#loadUserView').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#loadUserView').html(data).hide();
              $("#loadUserView").fadeIn(250);
        });
    });
    
    $(document).ready(function(e) {
        $.ajax({
            url         : "Templates/Erendora/Parts/SidePanel/Ajax/Ajax.Sidepanel.php", 
            beforeSend: function() {
                // setting a timeout
                $('#loadUserView').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#loadUserView').html(data).hide();
              $("#loadUserView").fadeIn(250);
        });
        
    });
    
    $(document).ready(function(e) {
        $.ajax({
            url         : "Templates/Erendora/Pages/Ingameaccounts/Ajax/Ajax.Construct.Accounts.php", 
            beforeSend: function() {
                // setting a timeout
                $('#loadIngameaccounts').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#loadIngameaccounts').html(data).hide();
              $("#loadIngameaccounts").fadeIn(250);
        });
        
    });
    
    $(document).ready(function(e) {
        $.ajax({
            url         : "Templates/Erendora/Pages/Support/Ajax/Ajax.Ticket.List.php", 
            beforeSend: function() {
                // setting a timeout
                $('#getTickets').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#getTickets').html(data).hide();
              $("#getTickets").fadeIn(250);
        });
        
    });
    
    $('#newTicket').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            url         : "Templates/Erendora/Pages/Support/Parts/Parts.Create.Ticket.php",
            beforeSend: function() {
                // setting a timeout
                $('#getTickets').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#getTickets').html(data).hide();
              $("#getTickets").fadeIn(250);
        });
    });
    
     $('#loadTicket').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            url         : "Templates/Erendora/Pages/Support/Ajax/Ajax.Ticket.List.php", 
            beforeSend: function() {
                // setting a timeout
                $('#getTickets').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#getTickets').html(data).hide();
              $("#getTickets").fadeIn(250);
        });
    });
    
    $(document).ready(function(e) {
        $.ajax({
            url         : "Templates/Erendora/Pages/Itemshop/Ajax/Ajax.Shop.Navigation.php", 
            beforeSend: function() {
                // setting a timeout
                $('#itemshopNavigation').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#itemshopNavigation').html(data).hide();
              $("#itemshopNavigation").fadeIn(250);
        });
        
    });
    
    $('#pscdonate').on('click', function(e) {
        e.preventDefault();
        var formData = {
            'pscvalue'  : $('#pscvalue').val(),
            'psc1'  : $('#psc1').val(),
            'psc2'  : $('#psc2').val(),
            'psc3'  : $('#psc3').val(),
            'psc4'  : $('#psc4').val(),
            'g-recaptcha-response'  : $('#g-recaptcha-response').val(),            
        };
        $.ajax({
            type        : "POST",
            url         : "Templates/Erendora/Pages/Donate/Ajax/Ajax.PSC.php", 
            data        : formData,
            beforeSend: function() {
                // setting a timeout
                $('#loadDonate').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#loadDonate').html(data).hide();
              $("#loadDonate").fadeIn(250);
        });
        
    });
    
});