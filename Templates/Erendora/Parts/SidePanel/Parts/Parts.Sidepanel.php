<div id="access"></div>
<div class="sideuserview">
	<div class="coinbadge">
		<ul class="left"><span class="coinvalue"><small><?= $object['coins']['donate'] ?></small></span><img data-toggle="tooltip" data-placement="bottom" src="<?= DEFAULT_TEMPLATE ?>Assets/img/Icons/Misc/coin_gold.png" title="Donate Coins"></ul>
		<ul class="right"><span class="coinvalue"><small><?= $object['coins']['vote'] ?></small></span><img data-toggle="tooltip" data-placement="bottom" src="<?= DEFAULT_TEMPLATE ?>Assets/img/Icons/Misc/coin_silver.png" title="Vote Coins"></ul>
	</div>
	<div class="clear"></div>
	<div class="itemlist">
		<ul class="left">
			<li><a href="<?= Validation::CreateUrl('Ingameaccounts') ?>">Ingame Accounts</a></li>
			<li><a href="<?= Validation::CreateUrl('Donate') ?>">Donate</a></li>
            <li><a href="<?= Validation::CreateUrl('Giftbox') ?>">Giftbox</a></li>
		</ul>
		<ul class="left">
			<?php 
            if($_SESSION['authority'] !== 'F'){
               if(isset($_SESSION['panelkey'])){
                    echo '<li><a href="/Admin/">Administration</a></li>';
                  }else {
                      echo '<li><a data-toggle="modal" data-target="#adminauthoritybox" href="#" id="getPanelAccess">Administration</a></li>
                    
                        <!-- Large modal -->
                        
                        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="adminauthoritybox">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                            <div class="pad15">
                                <input type="text" id="accessPanelKey" class="form-control">
                                <button type="button" id="checkPanelAccess" data-loading-text="Loading..." class="btn btn-warning marg25top" autocomplete="off">Check</button>
                            </div>
                            </div>
                        </div>
                        </div>';
                  }
            }
            ?>
             <li><a id="setvote" href="http://www.gtop100.com/topsites/Flyff/sitedetails/Fly-For-Sugbo-88581?vote=1<?php echo (isset($_SESSION['user']) ? '&pingUsername='.$_SESSION['user'] : '')?>" target="_blank">Vote</a></li>
			<li><a href="#">Options</a></li>
		</ul>
	</div>
	<div class="clear"></div>
</div>


<script>
	$('#checkPanelAccess').on('click', function(e) {
        e.preventDefault();
        var $btn = $(this).button('loading');
        var formData = {
            'accessPanelKey'  : $('#accessPanelKey').val(),
        };
        $.ajax({
            type        : "POST",
            url         : "Templates/Erendora/Parts/SidePanel/Parts/Parts.Panel.Access.php",
            data        : formData,
            beforeSend: function() {
                // setting a timeout
                $('#access').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#access').html(data).hide();
              $("#access").fadeIn(250);
              $btn.button('reset')
        });
    });
    $('#setvote').on('click', function(e) {
        $.ajax({
            url         : "Templates/Erendora/Parts/SidePanel/Ajax/Ajax.Vote.php",
            beforeSend: function() {
                // setting a timeout
                $('#access').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#access').html(data).hide();
              $("#access").fadeIn(250);
        });
    });
</script>