<ul class="sidebar list-group left">
	<li><?= $cfg['server_channel_name'] ?>:</li>
	<li>Server Time:</li>
	<div class="spacer"></div>
	<li>Player Online:</li>
	<li>Server Peak:</li>
	<div class="spacer"></div>
	<li>Experience:</li>
	<li>Itemdrop:</li>
	<li>Goldrop:</li>
	<div class="spacer"></div>
	<li><img data-toggle="tooltip" src="<?= DEFAULT_TEMPLATE ?>Assets/img/Icons/Ranking/lord.png" title="Server Lord" width="20%"> Lord:</li>
	<li><img data-toggle="tooltip" src="<?= DEFAULT_TEMPLATE ?>Assets/img/Icons/Ranking/mvp.png" title="Most Valuable Player" width="20%"> MVP:</li>
	<li><img data-toggle="tooltip" src="<?= DEFAULT_TEMPLATE ?>Assets/img/Icons/Ranking/Crown.png" title="Guild Siege Winner" width="20%"> Crown:</li>
	<div class="spacer"></div>
	<li>Teamspeak:</li>
</ul>
<ul class="sidebar-named list-group right">
	<li class="<?= strtolower($object['CheckChannel']) ?>"><?= $object['CheckChannel'] ?></li>
	<li><?= date('h:i a') ?></li>
	<div class="spacer"></div>
	<li><?= $object['OnlinePlayer'] ?></li>
	<li><?= $object['TotalOnlinePlayerAmount'] ?></li>
	<div class="spacer"></div>
	<li><?= $cfg['exp_rate'] ?></li>
	<li><?= $cfg['drop_rate'] ?></li>
	<li><?= $cfg['penya_rate'] ?></li>
	<div class="spacer"></div>
	<li><?= $object['ServerLord'] ?></li>
	<li><?= $object['MostViolentPlayer'] ?></li>
	<li><?= $object['GuildWarWinner'] ?></li>
	<div class="spacer"></div>
	<li><a href="ts3server://37.59.4.34:9987">Connect</a></li>
</ul>