<div class="mvp-ranking">
	<div class="left mvp-get">
		<span class="label" style="background:#b69d4f;">First Place</span>
		<div class="clear marg25bot"></div>
		<div class="left">
			<img src="<?= DEFAULT_TEMPLATE ?>Assets/img/Character/<?= Ranking::GetClassName($object[1]['m_nJob']) ?>.png" width="100">
		</div>
		<div class="left">
			<ul class="list-group">
				<li class="list-group-item"><b>Charactername:</b> <?= $object[1]['m_szName'] ?></li>
				<li class="list-group-item"><b>Level:</b> <?= $object[1]['m_nLevel'] ?></li>
				<li class="list-group-item"><b>Guild:</b> <?= $object[1]['m_szGuild'] ?></li>
				<li class="list-group-item"><b>Class:</b> <img data-toggle="tooltip" src="<?=  DEFAULT_TEMPLATE ?>Assets/img/Icons/Classes/<?= $object[1]['m_nJob'] ?>.png" title="<?= Ranking::GetClassIcon($object[1]['m_nJob']) ?>" width="25"></li>
			</ul>
		</div>
	</div>
	<div class="left mvp-get">
		<span class="label" style="background:#a7a7a7;">Second Place</span>
		<div class="clear marg25bot"></div>
		<div class="left">
			<img src="<?= DEFAULT_TEMPLATE ?>Assets/img/Character/<?= Ranking::GetClassName($object[2]['m_nJob']) ?>.png" width="100">
		</div>
		<div class="left">
			<ul class="list-group">
				<li class="list-group-item"><b>Charactername:</b> <?= $object[2]['m_szName'] ?></li>
				<li class="list-group-item"><b>Level:</b> <?= $object[2]['m_nLevel'] ?></li>
				<li class="list-group-item"><b>Guild:</b> <?= $object[2]['m_szGuild'] ?></li>
				<li class="list-group-item"><b>Class:</b> <img data-toggle="tooltip" src="<?=  DEFAULT_TEMPLATE ?>Assets/img/Icons/Classes/<?= $object[2]['m_nJob'] ?>.png" title="<?= Ranking::GetClassIcon($object[2]['m_nJob']) ?>" width="25"></li>
			</ul>
		</div>
	</div>
	<div class="left mvp-get">
		<span class="label" style="background:#b6634f;">Third Place</span>
		<div class="clear marg25bot"></div>
		<div class="left">
			<img src="<?= DEFAULT_TEMPLATE ?>Assets/img/Character/<?= Ranking::GetClassName($object[3]['m_nJob']) ?>.png" width="100">
		</div>
		<div class="left">
			<ul class="list-group">
				<li class="list-group-item"><b>Charactername:</b> <?= $object[3]['m_szName'] ?></li>
				<li class="list-group-item"><b>Level:</b> <?= $object[3]['m_nLevel'] ?></li>
				<li class="list-group-item"><b>Guild:</b> <?= $object[3]['m_szGuild'] ?></li>
				<li class="list-group-item"><b>Class:</b> <img data-toggle="tooltip" src="<?=  DEFAULT_TEMPLATE ?>Assets/img/Icons/Classes/<?= $object[3]['m_nJob'] ?>.png" title="<?= Ranking::GetClassIcon($object[3]['m_nJob']) ?>" width="25"></li>
			</ul>
		</div>
	</div>
	
</div>
<div class="clear"></div>
<table class="table row">
<thead>
	<td><b>Rank</b></td>
	<td><b>Character</b></td>
	<td><b>Level</b></td>
	<td><b>Class</b></td>
	<td><b>Guild</b></td>
	<td><b>Playtime</b></td>
	<td><b>Status</b></td>
</thead>
<?php 
for($i = 1;$i <= count($object)-1;$i++){ 
	echo	'<tr>
				<td>' . $i . '</td>
				<td>' . $object[$i]['m_szName'] . '</td>
				<td>' . $object[$i]['m_nLevel'] . '</td>
				<td><img data-toggle="tooltip" src="' . DEFAULT_TEMPLATE . 'Assets/img/Icons/Classes/' . $object[$i]['m_nJob'] . '.png" title="' . Ranking::GetClassIcon($object[$i]['m_nJob']) . '" width="25"></td>
				<td>' . $object[$i]['m_szGuild'] . '</td>
				<td>' . round($object[$i]['TotalPlayTime'] / 3600) . '</td>
				<td><img data-toggle="tooltip" src="' . DEFAULT_TEMPLATE . 'Assets/img/Icons/Ranking/Player' . $object[$i]['Multiserver'] . '.png" title="' . $object[$i]['Multiserver'] . '"></td>
			 </tr>';
}
?>
</table>