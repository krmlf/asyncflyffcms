<div class="row marg25top">
	<div class="col-md-2"></div>
	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row marg25top">
					<div class="col-md-10">
						<div class="btn-group" role="group" aria-label="...">
							<button type="button" class="btn btn-default" id="loadPlayerRanking">Player Ranking</button>
							<button type="button" class="btn btn-default">Guild Siege Ranking</button>
						</div>
					</div>
				</div>
				<div class="row marg25top">
					<div class="alert alert-info" role="alert">
						<b>Player Ranking</b><br>
						Here you can see all available players on the server who are not an admin or gamemaster.
					</div>
					<div class="col-md-12"  id="listplayerdefault">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>