<?php Validation::DynamicRefresh(Validation::CreateUrl('News'), Validation::Access()); if(Validation::Access()){die('Fatal Error');} ?>
<div class="row marg25top">
	<div class="col-md-2"></div>
	<div class="col-md-6">
		<div class="panel panel-default news-panel">
			<div class="panel-body">
				<div class="row marg25top">
					<div class="alert alert-info" role="alert">
						<b>Login</b><br>
						Get access to your account to manage your ingame accounts or something else.			
					</div>
					<div id="loadLogin"></div>
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<div class="container-fluid">
							<section class="container">
								<div class="container-page">			
									<form method="POST" class="col-md-6">
										<h3 class="dark-grey">Login</h3>
										
										<div class="form-group col-md-12">
											<label>Username</label>
											<input type="text" name="username" maxlength="20" class="form-control" id="user">
										</div>
										
										<div class="form-group col-md-12">
											<label>Password</label>
											<input type="password" name="password" class="form-control" id="password">
										</div>	
										<div class="col-md-6">
											<a href="#">Forgot your account?</a>	
										</div>
										<div class="form-group col-md-6">
											<button type="submit" name="register" class="btn btn-warning" id="setLogin">Login</button>
										</div>
									</form>
								</div>
							</section>
						</div>
					</div>
					<div class="col-md-1"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="panel panel-default pad15">
			<?php
				require_once($path.DEFAULT_TEMPLATE.'Construct/SidePanel.php'); 
				require_once($path.DEFAULT_TEMPLATE.'Construct/ServerStats.php'); 
				require_once($path.DEFAULT_TEMPLATE.'Construct/SmallRanking.php'); 
			?>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
					