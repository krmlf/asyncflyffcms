<script>
$(function () {
  $('[data-toggle="popover"]').popover()
})
</script>
<div class="panel panel-default" style="background:none;">
  <div class="panel-body">
    
	<?php 
	
		for($i = 1;$i <= $object['Rows'];$i++){
			echo '<div class="col-md-4">
						<div class="thumbnail">
							<div class="caption">
								<h5>' . $object['Result'][$i]['itemname'] . '</h5>
							</div>
							<img data-toggle="tooltip" src="' . DEFAULT_TEMPLATE . 'Assets/img/Icons/Items/' . $object['Result'][$i]['szIcon'] . '" title="' . $object['Result'][$i]['itemname'] . '">
							<div class="caption">
								<small><span><img data-toggle="tooltip" src="' . DEFAULT_TEMPLATE . 'Assets/img/Icons/Misc/' . $object['Result'][$i]['currencyIcon'] . '.png" title="' . $object['Result'][$i]['currencyDesc'] . '" class="pad15"></span>' . $object['Result'][$i]['price'] . '</small>
								<small><span data-toggle="tooltip" class="glyphicon glyphicon-dashboard pad15" title="Item Amount"></span>' . $object['Result'][$i]['count'] . '</small>
							</div>
							
							<div class="caption">
								<p><small>' . $object['Result'][$i]['description'] . '</small></p>
							</div>
							<div class="caption" style="background:#e5e5e5;">
								<select id="character' . $i . '" class="form-control">';
								
								for($c = 0;$c <= count($chars)-1;$c++){
									echo '<option value="' . $chars[$c] . '">' . $chars[$c] . '</option>';
								}
								
			echo				'</select>
								<div class="clear marg25bot"></div>
								<button id="buy' . $i . '" type="button" class="btn btn-warning left">Buy</button>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<script>
					$(\'#buy' . $i . '\').on(\'click\', function(e) {
						var formData = {
							\'secure\'  : \'' . $object['Result'][$i]['secure'] . '\',
							\'player\'  : $(\'#character' . $i . '\').val(),
						};
						$.ajax({
							type        : "POST",
							url         : "Templates/Erendora/Pages/Itemshop/Ajax/Ajax.Shop.Buy.php",
							data        : formData,
							beforeSend: function() {
								// setting a timeout
								$(\'#shopBuyed\').html(\'<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>\');
							},
						}).done(function(data) {
							$(\'#shopBuyed\').html(data).hide();
							$("#shopBuyed").fadeIn(250);
						});
						
					});</script>';
		}
	
	?>
	
  </div>
</div>