<thead>
	<td><b>#</b></td>
	<td><b>Description</b></td>
	<td><b>Mirror</b></td>
	<td><b>Size</b></td>
	<td><b>Registered</b></td>
	<td><b>Host</b></td>
</thead>
<?php
for($i = 1;$i <= $object['Rows'];$i++){
	echo	'<tr>
				<td># ' . $object['Result'][$i]['id'] .'</td>
				<td>' . $object['Result'][$i]['description'] . '</td>
				<td><a data-toggle="tooltip" href="' . $object['Result'][$i]['mirror'] . '" target="_blank" title="' . $object['Result'][$i]['mirror'] . '">Download <span class="glyphicon glyphicon-triangle-bottom"></span></a></td>
				<td>' . round($object['Result'][$i]['size'] / 1000, 2) . ' GB</td>
				<td>' . date('Y-m-d', $object['Result'][$i]['registered']) . '</td>
				<td>' . $object['Result'][$i]['host'] . '</td>
			</tr>';
}