<div class="row marg25top">
	<div class="col-md-2"></div>
	<div class="col-md-6">
		<div class="panel panel-default news-panel">
			<div class="panel-body">
				<div class="row marg25top">
					<div class="alert alert-info" role="alert">
						<b>Downloads</b><br>
						First of all we are joyful that you want to play on <b>Library of Wonderland</b>. Below this information box you can see some download links. Pick up one of them and getting started today.
					</div>
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<table class="table" id="listDownloads"></table>
					</div>
					<div class="col-md-1"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="panel panel-default pad15">
			<?php
				require_once($path.DEFAULT_TEMPLATE.'Construct/SidePanel.php'); 
				require_once(DEFAULT_TEMPLATE.'Construct/ServerStats.php'); 
				require_once(DEFAULT_TEMPLATE.'Construct/SmallRanking.php'); 
			?>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>