<div class="pad25lr marg25top">
	<?php 
	
		for($i = 0;$i <= count($object)-2;$i++){
			echo '<div class="panelitem marg25bot">
					<h4>' . $object[$i]['title'] . '<small><span class="label label-default news-date">' . $object[$i]['date'] . '</span></small></h4>
					<p>' . $object[$i]['text'] . '</p>
					<button type="button" class="news-link btn btn-warning" onclick="window.open(\'' . $object[$i]['link']  . '\')">Read more</button>
				</div>';
		}
	
	?>
</div>