<div class="row marg25top">
	<div class="col-md-2"></div>
	<div class="col-md-6">
		<div class="panel panel-default news-panel">
			<div class="panel-bodys">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a id="newsTab" href="#" aria-controls="news" role="tab" data-toggle="tab">News</a></li>
					<li role="presentation"><a id="eventsTab" href="#" aria-controls="events" role="tab" data-toggle="tab">Events</a></li>
					<li role="presentation"><a id="updatesTab" href="#" aria-controls="updates" role="tab" data-toggle="tab">Updates</a></li>
				</ul>
				
				<!-- Tab panes -->
				<div class="tab-content news">
					<div role="tabpanel" class="tab-panel active" id="getNews"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="panel panel-default pad15">
			<?php
				require_once($path.DEFAULT_TEMPLATE.'Construct/SidePanel.php'); 
				require_once($path.DEFAULT_TEMPLATE.'Construct/ServerStats.php'); 
				require_once($path.DEFAULT_TEMPLATE.'Construct/SmallRanking.php'); 
			?>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>