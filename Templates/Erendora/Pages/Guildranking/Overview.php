<div class="row marg25top">
	<div class="col-md-2"></div>
	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row marg25top">
					<div class="col-md-10">
						<div class="btn-group" role="group" aria-label="...">
							<button type="button" class="btn btn-default" id="loadGuildRanking">Guild Ranking</button>
							<button type="button" class="btn btn-default">Guild Siege Ranking</button>
						</div>
					</div>
				</div>
				<div class="row marg25top">
					<div class="alert alert-info" role="alert">
						<b>Guild Ranking</b><br>
						Get an overview about all guilds and their stats.
					</div>
					<div class="col-md-12" id="listguilddefault">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>