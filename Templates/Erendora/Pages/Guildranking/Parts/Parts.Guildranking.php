<table class="table row">
<thead>
	<td><b>Rank</b></td>
	<td><b>Guildname</b></td>
	<td><b>Guildlevel</b></td>
	<td><b>Creat Time</b></td>
	<td><b>Member Amount</b></td>
	<td><b>Leader</b></td>
</thead>
<?php 
$a = 1;
for($i = 0;$i <= $object['hide']-1;$i++){
	echo	'<tr>
				<td>' . $a++ . '</td>
				<td>' . $object[$i]['m_szGuild'] . '</td>
				<td>
					<div data-toggle="tooltip" class="progress"  title="Level: ' . $object[$i]['m_nLevel'] . ' von ' . intval($cfg['guild_max_level']) . '">
						<div class="progress-bar" role="progressbar" style="min-width: ' . ($object[$i]['m_nLevel'] /  intval($cfg['guild_max_level'])) * 100 . '%;background:#78d350;"></div>
					</div>
				</td>
				<td>'. 	$object[$i]['CreateTime'] .'</td>
				<td>
					<div data-toggle="tooltip" class="progress"  title="Gildenmember: ' . $object[$i]['MemberAmount'] . ' von ' . intval($cfg['guild_max_slots']) . '">
						<div class="progress-bar" role="progressbar" style="min-width: ' . ($object[$i]['MemberAmount'] /  intval($cfg['guild_max_slots'])) * 100 . '%;background:#' . $object[$i]['SlotSplitter'] . ';"></div>
					</div>
				</td>
				<td>' . $object[$i]['GuildLeader'] . '</td>
			 </tr>';
}
?>
</table>