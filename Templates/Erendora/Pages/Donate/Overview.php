<?php Validation::DynamicRefresh(Validation::CreateUrl('News'), !Validation::Access()); if(!Validation::Access()){die('Fatal Error');} ?>
<div class="row marg25top">
	<div class="col-md-2"></div>
	<div class="col-md-6">
		<div class="panel panel-default news-panel">
			<div class="panel-body">
				<div class="row marg25top">
					<div class="alert alert-info" role="alert">
						<b>Donate</b><br>
						If you want, you can spend some money to our server. Just choose a payment method and then submit the form.					
					</div>
					<div id="loadDonate"></div>
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<div class="container-fluid">
							<section class="container">
								<div class="container-page">			
									<form method="POST" class="col-md-6">
										<h3 class="dark-grey">Donate with Paysafecard</h3>
										
										<select id="pscvalue" class="form-group form-control col-md-12">
											<option value="10">10 €</option>
											<option value="20">20 €</option>
											<option value="25">25 €</option>
											<option value="50">50 €</option>
											<option value="100">100 €</option>
										</select>
										
										<div class="form-group col-md-3">
											<input type="text" maxlength="4" class="form-control" id="psc1" placeholder="0000">
										</div>
										<div class="form-group col-md-3">
											<input type="text" maxlength="4" class="form-control" id="psc2" placeholder="0000">
										</div>
										<div class="form-group col-md-3">
											<input type="text" maxlength="4" class="form-control" id="psc3" placeholder="0000">
										</div>
										<div class="form-group col-md-3">
											<input type="text" maxlength="4" class="form-control" id="psc4" placeholder="0000">
										</div>
										<div class="clear"></div>
											<div class="g-recaptcha" id="g-recaptcha-response" data-sitekey="<?= RECAPTCHA_PUBLIC_KEY ?>"></div>
										<div class="clear marg25bot"></div>		
										<div class="form-group col-md-6">
											<button type="submit" class="btn btn-warning" id="pscdonate">Donate now</button>
										</div>
									</form>
								</div>
							</section>
						</div>
					</div>
					<div class="col-md-1"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="panel panel-default pad15">
			<?php
				require_once($path.DEFAULT_TEMPLATE.'Construct/SidePanel.php'); 
				require_once($path.DEFAULT_TEMPLATE.'Construct/ServerStats.php'); 
				require_once($path.DEFAULT_TEMPLATE.'Construct/SmallRanking.php'); 
			?>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
					