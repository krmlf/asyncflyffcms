<?php Validation::DynamicRefresh(Validation::CreateUrl('News'), !Validation::Access()); if(!Validation::Access()){die('Fatal Error');} ?>
<div class="row marg25top">
	<div class="col-md-2"></div>
	<div class="col-md-6">
		<div class="panel panel-default news-panel">
			<div class="panel-body">
				<div class="row marg25top">
					<div class="alert alert-info" role="alert">
						<b>Ingame Accounts</b><br>
						If you would like to connect with our gameserver, just create some ingame accounts right here.
					</div>
					<div class="col-md-12" id="loadIngameaccounts">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="panel panel-default pad15">
			<?php
				require_once($path.DEFAULT_TEMPLATE.'Construct/SidePanel.php'); 
				require_once($path.DEFAULT_TEMPLATE.'Construct/ServerStats.php'); 
				require_once($path.DEFAULT_TEMPLATE.'Construct/SmallRanking.php'); 
			?>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>