<span class="sr-only" style="position:relative;color:#000;top:12px;">Available account slots: <?= intval($cfg['ingame_account_limit'])-count($object['ingameaccounts']); ?></span>
<div class="progress marg25top" style="height:40px;">
  <div class="progress-bar progress-bar-danger" style="width: <?= count($object['ingameaccounts'])/intval($cfg['ingame_account_limit'])*100 ?>%;">
  </div>
  <div class="progress-bar progress-bar-success" style="width: <?= (intval($cfg['ingame_account_limit'])-count($object['ingameaccounts']))/$cfg['ingame_account_limit']*100 ?>%;">
  </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Create an ingame account</h4>
      </div>
      <div id="loadIngameAccount"></div>
      <section class="container">
        <div class="container-page">			
          <form method="POST" class="col-md-6">
            
            <div class="form-group col-md-12">
              <label>Accountname</label>
              <input type="text" name="username" maxlength="12" class="form-control" id="user">
            </div>
            
            <div class="form-group col-md-6">
              <label>Password</label>
              <input type="password" name="password" class="form-control" id="password">
            </div>
            
            <div class="form-group col-md-6">
              <label>Repeat Password</label>
              <input type="password" name="password2" class="form-control" id="password2">
            </div>
            
            <div class="clear"></div>
              <div class="g-recaptcha" id="g-recaptcha-response" data-sitekey="<?= RECAPTCHA_PUBLIC_KEY ?>"></div>
            <div class="clear marg25bot"></div>		
            <div class="form-group col-md-6">
              <button type="submit" name="register" id="createIngameAccount" class="btn btn-warning" id="setRegistration">Register your ingame account</button>
            </div>
          </form>
        </div>
      </section>
    </div>
  </div>
</div>
<div class="clear marg25bot"></div>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
  $('#createIngameAccount').on('click', function(e) {
        e.preventDefault();
        var formData = {
            'user'  : $('#user').val(),
            'password'  : $('#password').val(),
            'password2'  : $('#password2').val(),
            'g-recaptcha-response'  : $('#g-recaptcha-response').val(),
        };
        $.ajax({
            type        : "POST",
            url         : "Templates/Erendora/Pages/Ingameaccounts/Ajax/Ajax.Create.Account.php",
            data        : formData,
            beforeSend: function() {
                // setting a timeout
                $('#loadIngameAccount').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#loadIngameAccount').html(data).hide();
              $("#loadIngameAccount").fadeIn(250);
        });
    });
</script>
<div class="panel-group" id="accordion" role="tab" aria-multiselectable="true">
<?php
if($object['ingameaccounts']){
  for($accountValue = 0;$accountValue <= count($object['ingameaccounts'])-1;$accountValue++){
        echo '<div class="panel panel-default">
              <div class="panel-heading" role="tab" id="heading' . $accountValue . '">
                <h4 class="panel-title">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse' . $accountValue . '" aria-expanded="false" aria-controls="collapse' . $accountValue . '">
                    <div class="row">
                        <div class="col-md-1"><small><b>#' . $accountValue . '</b></small></div>
                        <div class="col-md-2"><small>Account <b><br>' . $object['ingameaccounts'][$accountValue] . '</b></small></div>';
                      
                      
                        /**if(isset($object['chractercredentials'][$accountValue])){
                          for($slotValue = 1;$slotValue <= count($object['chractercredentials'][$accountValue]);$slotValue++){
                            if($object['chractercredentials'][$accountValue][$slotValue] > 0){
                              echo '<div class="col-md-2"><img src="' . DEFAULT_TEMPLATE . 'Assets/img/Icons/Classes/' . $object['chractercredentials'][$accountValue][$slotValue]['info']['class']['icon']  . '.png" alt="" width="40" data-toggle="tooltip" title="' . $object['chractercredentials'][$accountValue][$slotValue]['info']['class']['name'] . '"><br><small>Slot ' . $slotValue . ': <b><br>' . $object['chractercredentials'][$accountValue][$slotValue]['name'] . '</b></small></div>';
                            }else {
                              continue;
                            }
                          } 
                        }*/
                        
         echo '       <div class="col-md-2"><small>Creation Time: <b><br>' . substr($object['CreationDate'][$accountValue], 0, 10) . '</b></small></div>
                   </div>
                  </a>
                </h4>
              </div>
              <div id="collapse' . $accountValue . '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' . $accountValue . '">
                <div class="panel-body">
                  <div class="itemlist marg25top marg25bot">
                    <ul class="left">
                      <li><a href="#">Show character details</a></li>
                      <li><a  href="#">Deleted character history</a></li>
                    </ul>
                    <ul class="left">
                      <li><a  href="#">Options</a></li>
                    </ul>
                    <ul class="left">
                      <li><a  href="#">Analytics</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>';
  }
}
?>
  </div>
</div>
