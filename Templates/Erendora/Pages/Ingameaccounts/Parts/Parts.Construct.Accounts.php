<div class="ingameaccounts">
    <?php 
    
        if($object['ingameaccounts'] === null){
            echo '<div class="alert alert-warning" role="alert">
                <b>Notice</b><br>
                There are acutally no ingame accounts found. If you miss one of your accounts please visit our support page.
            </div>';
        }
        
        ?>
        
        <!-- Small modal -->
        <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bs-example-modal-lg">Register a new ingame account</button>
        <button type="button" class="btn btn-warning" id="refreshAccountList">Refresh Account List</button>
        <button type="button" class="btn btn-primary" id="recoverAccount">Recover an ingame account</button>
        
        <div id="ingameaccountview"></div>
</div>


<script>
    $(document).ready(function(e) {
        $.ajax({
            url         : "Templates/Erendora/Pages/Ingameaccounts/Ajax/Ajax.Accounts.View.php", 
            beforeSend: function() {
                // setting a timeout
                $('#ingameaccountview').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#ingameaccountview').html(data).hide();
              $("#ingameaccountview").fadeIn(250);
        });
        
    });
    $('#refreshAccountList').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            url         : "Templates/Erendora/Pages/Ingameaccounts/Ajax/Ajax.Accounts.View.php",  
            beforeSend: function() {
                // setting a timeout
                $('#ingameaccountview').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
            $('#ingameaccountview').html(data).hide();
            $("#ingameaccountview").fadeIn(250);
        });
    });
    $('#recoverAccount').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            url         : "Templates/Erendora/Pages/Ingameaccounts/Parts/Parts.Recover.Account.php", 
            beforeSend: function() {
                // setting a timeout
                $('#ingameaccountview').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
            $('#ingameaccountview').html(data).hide();
            $("#ingameaccountview").fadeIn(250);
        });
    });
</script>