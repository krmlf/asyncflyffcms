<?php 
session_start();
require_once($_SERVER['DOCUMENT_ROOT'].'/Source/Includes/Config.php');
?>
<div id="recoverInfoContainer"></div>
<section class="container">
  <div class="container-page marg25top">			
    <form method="POST" class="col-md-6">
      
      <div class="form-group col-md-12">
        <label>Accountname</label>
        <input type="text" name="username" maxlength="12" class="form-control" id="user">
      </div>
      
      <div class="form-group col-md-12">
        <label>Password</label>
        <input type="password" name="password" class="form-control" id="password">
      </div>
      
      <div class="clear"></div>
        <div class="g-recaptcha" id="g-recaptcha-response" data-sitekey="<?= RECAPTCHA_PUBLIC_KEY ?>"></div>
      <div class="clear marg25bot"></div>		
      <div class="form-group col-md-6">
        <button type="submit" name="register" id="recoverYourAccount" class="btn btn-warning" id="setRegistration">Recover your ingame account</button>
      </div>
    </form>
  </div>
</section>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
  $('#recoverYourAccount').on('click', function(e) {
        e.preventDefault();
        var formData = {
            'user'  : $('#user').val(),
            'password'  : $('#password').val(),
            'g-recaptcha-response'  : $('#g-recaptcha-response').val(),
        };
        $.ajax({
            type        : "POST",
            url         : "Templates/Erendora/Pages/Ingameaccounts/Ajax/Ajax.Recover.Account.php",
            data        : formData,
            beforeSend: function() {
                // setting a timeout
                $('#recoverInfoContainer').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#recoverInfoContainer').html(data).hide();
              $("#recoverInfoContainer").fadeIn(250);
        });
    });
</script>
      