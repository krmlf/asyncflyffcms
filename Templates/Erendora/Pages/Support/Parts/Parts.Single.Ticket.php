<input type="hidden" id="thirdTicketID" value="<?= $object['ticket']['id'] ?>">
<div class="alert alert-info" role="alert">
	<b>Ticket #<?php $post = Validation::ValidateInput($_POST); echo $post['ticketid'] ?></b>
</div>
<div class="row">
	
	<div class="col-md-6">
		<ul class="list-group">
			<li class="list-group-item">Ticketnumber: <b><?= $object['ticket']['id'] ?></b></li>
			<li class="list-group-item">Date: <b><?= date('D d.M.Y', $object['ticket']['date']) ?></b></li>
			<li class="list-group-item">Author: <b><?= $object['ticket']['author'] ?></b></li>
		</ul>
	</div>
	
	<div class="col-md-6">
		<ul class="list-group support">
			<li class="list-group-item">State: <b class="<?= str_replace(' ', '', strtolower($object['state'])) ?>"><?= $object['ticket']['state'] ?></b></li>
			<li class="list-group-item">Comments: <b><?= $object['ticket']['comments'] ?></b></li>
			<li class="list-group-item">Moderating opponent: <b><?= $object['ticket']['moderator'] ?></b></li>
		</ul>
	</div>
	
	<div class="col-md-12">
		<ul class="list-group support">
			<li class="list-group-item"><b><?= $object['ticket']['title'] ?></b></li>
		</ul>
	</div>
	
	<div class="col-md-12">
		<ul class="list-group support">
			<li class="list-group-item"><p><?= nl2br($object['ticket']['text']) ?></p></li>
		</ul>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<form method="POST">
				
				<div class="form-group">
                    <label>Submit a comment</label>
                    <textarea name="text" id="commenttext" name="commenttext" class="form-control"></textarea>
                </div>
					
				<div class="clear"></div>
					<div class="g-recaptcha" id="g-recaptcha-response" data-sitekey="<?= RECAPTCHA_PUBLIC_KEY ?>"></div>
				<div class="clear marg25bot"></div>		
				<div class="form-group col-md-6">
					<button type="submit" name="savecomment" class="btn btn-warning" id="savecomment">Save Comment</button>
				</div>
				
			</form>
		</div>
	</div>
	<div id="insertcomment"></div>
	<div id="ticketcomment"></div>
	
</div>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
	$('#savecomment').on('click', function(e) {
        e.preventDefault();
        var formData = {
            'id'  : $('#thirdTicketID').val(),
            'commenttext'  : $('#commenttext').val(),
            'g-recaptcha-response'  : $('#g-recaptcha-response').val(),
        };
        $.ajax({
            type        : "POST",
            url         : "Templates/Erendora/Pages/Support/Ajax/Ajax.Add.Comment.php",
            data        : formData,
            beforeSend: function() {
                // setting a timeout
                $('#insertcomment').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#insertcomment').html(data).hide();
              $("#insertcomment").fadeIn(250);
              $.ajax({
                    type        : "POST",
                    url         : "Templates/Erendora/Pages/Support/Ajax/Ajax.View.Comments.php",
                    data        : formData,
                    beforeSend: function() {
                        // setting a timeout
                        $('#ticketcomment').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
                    },
                }).done(function(data) {
                    $('#ticketcomment').html(data).hide();
                    $("#ticketcomment").fadeIn(250);
                });
        });
    });
	
	$(document).ready(function(e) {
        var formData = {
            'id'  : $('#thirdTicketID').val(),
        };
        $.ajax({
            type        : "POST",
            url         : "Templates/Erendora/Pages/Support/Ajax/Ajax.View.Comments.php", 
            data        : formData,
            beforeSend: function() {
                // setting a timeout
                $('#ticketcomment').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#ticketcomment').html(data).hide();
              $("#ticketcomment").fadeIn(250);
        });
        
    });
</script>