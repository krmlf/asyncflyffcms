<?php require_once($_SERVER['DOCUMENT_ROOT'].'/Source/Includes/Config.php'); ?>
<div id="getNewTicketResponse"></div>
<div class="container-fluid">
	<section class="container">
		<!-- Tab panels -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-panel active">
				<form method="POST" class="col-md-6">
					<h3 class="dark-grey">Create a ticket</h3>
					
					<div class="form-group col-md-12">
						<label>Name your ticket</label>
						<input type="text" name="title" maxlength="255" class="form-control" id="title">
					</div>
					
					<div class="form-group col-md-12">
						<label>Tell us your desire</label>
						<textarea name="text" id="text" class="form-control"></textarea>
					</div>
						
					<div class="clear"></div>
					<div class="g-recaptcha" id="g-recaptcha-response" data-sitekey="<?= RECAPTCHA_PUBLIC_KEY ?>"></div>
					<div class="clear marg25bot"></div>		
					<div class="form-group col-md-6">
					<button type="submit" name="writeticket" id="writeticket" class="btn btn-warning" id="setRegistration">Create a ticket</button>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
	$('#writeticket').on('click', function(e) {
        e.preventDefault();
		var formData = {
            'title'  : $('#title').val(),
            'text'  : $('#text').val(),
            'g-recaptcha-response'  : $('#g-recaptcha-response').val(),
        };
        $.ajax({
			type		: "POST",
            url         : "Templates/Erendora/Pages/Support/Ajax/Ajax.Create.Ticket.php",
			data        : formData,
            beforeSend: function() {
                // setting a timeout
                $('#getNewTicketResponse').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#getNewTicketResponse').html(data).hide();
              $("#getNewTicketResponse").fadeIn(250);
        });
    });
</script>