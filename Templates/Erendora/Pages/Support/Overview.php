<?php Validation::DynamicRefresh(Validation::CreateUrl('News'), !Validation::Access()); if(!Validation::Access()){die('Fatal Error');} ?>
<div class="row marg25top">
	<div class="col-md-2"></div>
	<div class="col-md-6">
		<div class="panel panel-default news-panel">
			<div class="panel-body">
				<div class="alert alert-info" role="alert">
					<b>Support</b><br>
					Here you can create a support ticket to share your desire with the <b>Library of Wonderland Team</b>.
				</div>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a id="loadTicket" href="#" aria-controls="events" role="tab" data-toggle="tab">Show tickets</a></li>
					<li role="presentation"><a id="newTicket" href="#" aria-controls="news" role="tab" data-toggle="tab">Create a ticket</a></li>
				</ul>
				
				<div id="getTickets"></div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="panel panel-default pad15">
			<?php
				require_once($path.DEFAULT_TEMPLATE.'Construct/SidePanel.php'); 
				require_once($path.DEFAULT_TEMPLATE.'Construct/ServerStats.php'); 
				require_once($path.DEFAULT_TEMPLATE.'Construct/SmallRanking.php'); 
			?>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>