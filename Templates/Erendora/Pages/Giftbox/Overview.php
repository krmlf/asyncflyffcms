<div class="row marg25top">
	<div class="col-md-2"></div>
	<div class="col-md-6">
		<div class="panel panel-default news-panel">
			<div class="panel-body">
				<div id="getGiftboxList"></div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="panel panel-default pad15">
			<?php
				require_once($path.DEFAULT_TEMPLATE.'Construct/SidePanel.php'); 
				require_once($path.DEFAULT_TEMPLATE.'Construct/ServerStats.php'); 
				require_once($path.DEFAULT_TEMPLATE.'Construct/SmallRanking.php'); 
			?>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>