<div class="row">
	<div class="col-xs-2"></div>
	<div class="col-xs-8">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				</div>
			
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a class="<?= Nav::Link('News') ?>" href="<?= Validation::CreateUrl('News') ?>"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> News</a></li>
					<li><a href="http://www.library-of-wonderland.com/forum/" target="_blank"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Forum</a></li>
					<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-equalizer" aria-hidden="true"></span> Ranking <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a class="<?= Nav::Link('Guildranking'); ?>" href="<?= Validation::CreateUrl('Guildranking') ?>">Guild Ranking</a></li>
							<li><a class="<?= Nav::Link('Playerranking'); ?>" href="<?= Validation::CreateUrl('Playerranking') ?>">Player Ranking</a></li>
						</ul>
					</li>
					<li><a class="<?= Nav::Link('Itemshop'); ?>" href="<?= Validation::CreateUrl('Itemshop') ?>"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Itemshop</a></li>
					<li><a class="<?= Nav::Link('Download'); ?>" href="<?= Validation::CreateUrl('Download') ?>"><span class="glyphicon glyphicon-circle-arrow-down" aria-hidden="true"></span> Download</a></li>
					<li><a class="<?= Nav::Link('Support'); ?>" href="<?= Validation::CreateUrl('Support') ?>"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> Support</a></li>
					<?php 
						if(!Validation::Access()){
							echo '<li><a class="' . Nav::Link('Signup') . '" href="' . Validation::CreateUrl('Signup') . '"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Registration</a></li>';
							echo '<li><a class="' . Nav::Link('Login') . '" href="' . Validation::CreateUrl('Login') . '"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> Login</a></li>';
						}else {
							echo '<li><a href="?logout=true"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> Logout</a></li>';
						}
					?>
				</ul>
				</div>
			</div>
		</nav>
	</div>
	<div class="col-xs-2"></div>
</div>