<?php
session_start();
require_once('Source/Includes/Config.php');
Common::Logout();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="<?= $cfg['web_author'] ?>">
    <meta name="description" content="<?= $cfg['web_desc_panel'] ?>">
    <meta name="keywords" content="<?= $cfg['web_keywords'] ?>">
	
    <title><?= $cfg['web_title'] ?> - Admin Studio v<?= $cfg['admincp_version'] ?></title>
    <!-- Theme .css files -->
    <?php require_once('Source/Includes/StyleIncludes.php'); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <?php require_once('Source/Includes/ScriptIncludes.php'); ?>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="row">
      <?php
          //load dynamic site content
          require_once($path . DEFAULT_TEMPLATE . 'Construct/Headbar.php');
        ?>
    </div>
    <div class="row">
      
      <div class="col-md-2">
        <?php
          //load dynamic site content
          require_once($path . DEFAULT_TEMPLATE . 'Construct/Navigation.php');
        ?>
      </div>
      
      <div class="col-md-10">
        <?php
          //load dynamic site content
          require_once($path . 'Source/Includes/Site.php');
        ?>
      </div>
      
    </div>
  </body>
</html>