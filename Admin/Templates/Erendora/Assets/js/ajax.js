$(document).ready(function() {
    
    $('#loadWebconfigfile').on('click', function(e) {
        e.preventDefault();
        var $btn = $(this).button('loading');
        $.ajax({
            url         : "Templates/Erendora/Pages/Webconfig/Ajax/Ajax.getConfig.php", 
            beforeSend: function() {
                // setting a timeout
                $('#getNews').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#showWebConfigfile').html(data).hide();
              $("#showWebConfigfile").fadeIn(250);
              $btn.button('reset');
        });
        
    });
    
    $('#generateSecurityKey').on('click', function(e) {
        e.preventDefault();
        var $btn = $(this).button('loading');
        $.ajax({
            url         : "Templates/Erendora/Pages/Securitykeygen/Ajax/Ajax.Generate.Key.php", 
            beforeSend: function() {
                // setting a timeout
                $('#showSecurityKey').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#showSecurityKey').html(data).hide();
              $("#showSecurityKey").fadeIn(250);
              $btn.button('reset');
        });
        
    });
    
    $('#getAllSecuritykeys').on('click', function(e) {
        e.preventDefault();
        var $btn = $(this).button('loading');
        $.ajax({
            url         : "Templates/Erendora/Pages/Securitykeygen/Ajax/Ajax.Get.Key.php", 
            beforeSend: function() {
                // setting a timeout
                $('#showSecurityKey').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#showSecurityKey').html(data).hide();
              $("#showSecurityKey").fadeIn(250);
              $btn.button('reset');
        });
        
    });
    
    $('#getFullItemList').on('click', function(e) {
        e.preventDefault();
        var formData = {
           'itemRows'  : $('#itemRows').val(),
        };
        var $btn = $(this).button('loading');
        $.ajax({
            type        : "POST",
            url         : "Templates/Erendora/Pages/Itemwiki/Ajax/Ajax.Get.Itemlist.php",
            data        : formData,
            beforeSend: function() {
                // setting a timeout
                $('#showItemResult').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#showItemResult').html(data).hide();
              $("#showItemResult").fadeIn(250);
              $btn.button('reset');
        });
        
    });
    
    $('#getShoplist').on('click', function(e) {
        e.preventDefault();
        var $btn = $(this).button('loading');
        $.ajax({
            url         : "Templates/Erendora/Pages/Shoplist/Ajax/Ajax.Get.Shoplist.php", 
            beforeSend: function() {
                // setting a timeout
                $('#showShoplist').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#showShoplist').html(data).hide();
              $("#showShoplist").fadeIn(250);
              $btn.button('reset');
        });
        
    });
    
    $('#loadTicket').on('click', function(e) {
        e.preventDefault();
        var $btn = $(this).button('loading');
        $.ajax({
            url         : "Templates/Erendora/Pages/Tickets/Ajax/Ajax.ListTickets.php", 
            beforeSend: function() {
                // setting a timeout
                $('#getTickets').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#getTickets').html(data).hide();
              $("#getTickets").fadeIn(250);
              $btn.button('reset');
        });
        
    });
    
    $('#searchItemByCredentials').on('click', function(e) {
        e.preventDefault();
        var formData = {
           'itemValue'  : $('#itemValue').val(),
        };
        var $btn = $(this).button('loading');
        $.ajax({
            type        : "POST",
            url         : "Templates/Erendora/Pages/Itemwiki/Ajax/Ajax.Search.Item.php", 
            data        : formData,
            beforeSend: function() {
                // setting a timeout
                $('#showItemResult').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#showItemResult').html(data).hide();
              $("#showItemResult").fadeIn(250);
              $btn.button('reset');
        });
        
    });
    
    $('#addDownloads').on('click', function(e) {
        e.preventDefault();
        var formData = {
           'itemValue'  : $('#itemValue').val(),
        };
        var $btn = $(this).button('loading');
        $.ajax({
            type        : "POST",
            url         : "Templates/Erendora/Pages/Downloads/Parts/Parts.Add.Downloads.php", 
            data        : formData,
            beforeSend: function() {
                // setting a timeout
                $('#showDownloads').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#showDownloads').html(data).hide();
              $("#showDownloads").fadeIn(250);
              $btn.button('reset');
        });
        
    });
    
    $('#loadDownloads').on('click', function(e) {
        e.preventDefault();
        var $btn = $(this).button('loading');
        $.ajax({
            url         : "Templates/Erendora/Pages/Downloads/Ajax/Ajax.Get.Downloads.php", 
            beforeSend: function() {
                // setting a timeout
                $('#showDownloads').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#showDownloads').html(data).hide();
              $("#showDownloads").fadeIn(250);
              $btn.button('reset');
        });
        
    });
    
    $('#loadDonatesPSC').on('click', function(e) {
        e.preventDefault();
        var $btn = $(this).button('loading');
        $.ajax({
            url         : "Templates/Erendora/Pages/Donate/Ajax/Ajax.Get.DonatesPSC.php", 
            beforeSend: function() {
                // setting a timeout
                $('#showDonatesPSC').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#showDonatesPSC').html(data).hide();
              $("#showDonatesPSC").fadeIn(250);
              $btn.button('reset');
        });
        
    });
    
    $('#loadParseAccountRows').on('click', function(e) {
        e.preventDefault();
        var $btn = $(this).button('loading');
        $.ajax({
            url         : "Templates/Erendora/Pages/ParseOldAccounts/Ajax/Ajax.ParseAccounts.php", 
            beforeSend: function() {
                // setting a timeout
                $('#getParseAccountRows').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#getParseAccountRows').html(data).hide();
              $("#getParseAccountRows").fadeIn(250);
              $btn.button('reset');
        });
        
    });
    
});