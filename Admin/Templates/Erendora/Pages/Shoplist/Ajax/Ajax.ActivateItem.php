<?php 
session_start();
require_once($_SERVER['DOCUMENT_ROOT'].'/Admin/Source/Includes/Config.php');
$controller = new Shop();
$object = $controller->ActivateItem($_POST);
if((int)$_POST['state'] === 1){
	echo		'<button type="button" id="activateItem' . (int)$_POST['count'] . '" data-loading-text="Loading..." class="btn btn-success" autocomplete="off" value="0">Activated</button>';
}else {
	echo		'<button type="button" id="activateItem' . (int)$_POST['count'] . '" data-loading-text="Loading..." class="btn btn-danger" autocomplete="off" value="1">Deactivated</button>';
}
echo '<script>
		$(\'#activateItem' . (int)$_POST['count'] . '\').on(\'click\', function(e) {
			e.preventDefault();
			var formData = {
				\'itemid\'  : \'' . (int)$_POST['itemid'] . '\',
				\'state\'  : $(\'#activateItem' .(int)$_POST['count'] . '\').val(),
				\'count\'  : \'' . (int)$_POST['count'] . '\',
			};
			var $btn = $(this).button(\'loading\');
			$.ajax({
				type        : "POST",
				url         : "Templates/Erendora/Pages/Shoplist/Ajax/Ajax.ActivateItem.php", 
				data        : formData,
				beforeSend: function() {
					// setting a timeout
					$(\'#showShopState' . (int)$_POST['count'] . '\').html(\'<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>\');
				},
				}).done(function(data) {
					$(\'#showShopState' . (int)$_POST['count'] . '\').html(data).hide();
					$("#showShopState' . (int)$_POST['count'] . '").fadeIn(250);
			});
			
		});
</script>';