<div class="clear marg25top marg25bot"></div>
<div id="checkShopUpdateItem"></div>
<div style="overflow:scroll;max-height:500px;">
	<table class="table table-hover">
		<thead>
			<td><b>ID</b></td>
			<td><b>Name</b></td>
			<td><b>Image</b></td>
			<td><b>Amount</b></td>
			<td><b>Price</b></td>
			<td><b>Category</b></td>
			<td><b>Description</b></td>
			<td><b>Currency</b></td>
			<td><b>State</b></td>
		</thead>
		<?php
			for($i = 1;$i <= count($object['Result']);$i++){
				echo	'<tr>
							<td class="col-md-1" id="itemid' . $i . '">#' . $object['Result'][$i]['itemid'] . '</td>
							<td class="col-md-3">' . $object['Result'][$i]['itemname'] . '</td>
							<td class="col-md-1"><img data-toggle="tooltip" src="' . DEFAULT_TEMPLATE . 'Assets/img/Icons/Items/' . $object['Result'][$i]['szIcon'] . '" title="' . $object['Result'][$i]['itemname'] . '"></td>
							<td class="col-md-1">
								<div class="input-group">
									<input class="form-control" type="text" id="count' . $i . '" value="' . $object['Result'][$i]['count'] . '">
								</div>
							</td>
							<td class="col-md-1">
								<div class="input-group">
									<input class="form-control" type="text" id="price' . $i . '" value="' . $object['Result'][$i]['price'] . '">
								</div>
							</td>
							<td class="col-md-1">
								<div class="input-group">
									<select id="category' . $i . '" class="form-control">';
										echo '<option value="' .  $object['Result'][$i]['category'] . '">' .  $object['Result'][$i]['category'] . '</option>';
										for($cateID = 1;$cateID <= count($object['Categorys']);$cateID++){
											echo '<option value="' .  $object['Categorys'][$cateID]['category'] . '" style="color:red;">' .  $object['Categorys'][$cateID]['category'] . '</option>';
										}
										
				echo				'</select>
								</div>
							</td>
							<td class="col-md-1">
								<div class="input-group">
									<textarea class="form-control" id="desc' . $i . '">' . $object['Result'][$i]['description'] . '</textarea>
								</div>
							</td>
							<td class="col-md-1">
								<div class="input-group">
									<select id="currency' . $i . '" class="form-control">
										<option value="' .  $object['Result'][$i]['currency'] . '">' .  $object['Result'][$i]['currency'] . '</option>
										<option value="vCoins" style="color:red;">vCoins</option>
										<option value="dCoins" style="color:red;">dCoins</option>
									</select>
								</div>
							</td>
							<td class="col-md-1"><button type="button" id="updateItemToShop' . $i . '" data-loading-text="Loading..." class="btn btn-warning" autocomplete="off">Save</button></td>
							<td class="col-md-1" id="showShopState' . $i . '">';
				
				if(intval($object['Result'][$i]['active']) === 1){
					echo		'<button type="button" id="activateItem' . $i . '" data-loading-text="Loading..." class="btn btn-success" autocomplete="off" value="0">Activated</button>';
				}else {
					echo		'<button type="button" id="activateItem' . $i . '" data-loading-text="Loading..." class="btn btn-danger" autocomplete="off" value="1">Deactivated</button>';
				}
				
				echo		'</td>
							<td class="col-md-1"><button type="button" id="deleteItemFromShop' . $i . '" data-loading-text="Loading..." class="btn btn-danger" autocomplete="off" value="1">Delete</button></td>';
				
				echo	'</tr><script>
							$(\'#updateItemToShop' . $i . '\').on(\'click\', function(e) {
									e.preventDefault();
									var formData = {
										\'itemid\'  : \'' . $object['Result'][$i]['itemid'] . '\',
										\'count\'  : $(\'#count' . $i . '\').val(),
										\'price\'  : $(\'#price' . $i . '\').val(),
										\'desc\'  : $(\'#desc' . $i . '\').val(),
										\'currency\'  : $(\'#currency' . $i . '\').val(),
										\'category\'  : $(\'#category' . $i . '\').val(),
									};
									var $btn = $(this).button(\'loading\');
									$.ajax({
										type        : "POST",
										url         : "Templates/Erendora/Pages/Shoplist/Ajax/Ajax.UpdateToShop.php", 
										data        : formData,
										beforeSend: function() {
											// setting a timeout
											$(\'#checkShopUpdateItem\').html(\'<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>\');
										},
									}).done(function(data) {
										$(\'#checkShopUpdateItem\').html(data).hide();
										$("#checkShopUpdateItem").fadeIn(250);
										$btn.button(\'reset\');
									});
									
								});
								$(\'#deleteItemFromShop' . $i . '\').on(\'click\', function(e) {
									e.preventDefault();
									var formData = {
										\'itemid\'  : \'' . $object['Result'][$i]['itemid'] . '\',
									};
									var $btn = $(this).button(\'loading\');
									$.ajax({
										type        : "POST",
										url         : "Templates/Erendora/Pages/Shoplist/Ajax/Ajax.DeleteItem.php", 
										data        : formData,
										beforeSend: function() {
											// setting a timeout
											$(\'#checkShopUpdateItem\').html(\'<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>\');
										},
									}).done(function(data) {
										$(\'#checkShopUpdateItem\').html(data).hide();
										$("#checkShopUpdateItem").fadeIn(250);
										$btn.button(\'reset\');
										$.ajax({
										url         : "Templates/Erendora/Pages/Shoplist/Ajax/Ajax.Get.Shoplist.php", 
										beforeSend: function() {
											// setting a timeout
											$(\'#showShoplist\').html(\'<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>\');
										},
									}).done(function(data) {
										$(\'#showShoplist\').html(data).hide();
										$("#showShoplist").fadeIn(250);
									});
									});
									
								});
								$(\'#activateItem' . $i . '\').on(\'click\', function(e) {
									e.preventDefault();
									var formData = {
										\'itemid\'  : \'' . $object['Result'][$i]['itemid'] . '\',
										\'count\'  : \'' . $i . '\',
										\'state\'  : $(\'#activateItem' . $i . '\').val(),
									};
									var $btn = $(this).button(\'loading\');
									$.ajax({
										type        : "POST",
										url         : "Templates/Erendora/Pages/Shoplist/Ajax/Ajax.ActivateItem.php", 
										data        : formData,
										beforeSend: function() {
											// setting a timeout
											$(\'#showShopState' . $i . '\').html(\'<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>\');
										},
										}).done(function(data) {
											$(\'#showShopState' . $i . '\').html(data).hide();
											$("#showShopState' . $i . '").fadeIn(250);
									});
									
								});
							</script>';
			}
		
		?>
	</table>
</div>