<div class="alert alert-warning alert-dismissible marg25bot" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <b>Shop List</b><br>
  Manage your website shop by defining prices, item amounts and many more.
</div>

<button type="button" id="getShoplist" data-loading-text="Loading..." class="btn btn-primary" autocomplete="off">Load Shoplist</button>

<div id="showShoplist"></div>