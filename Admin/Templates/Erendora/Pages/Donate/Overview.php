<div class="alert alert-warning alert-dismissible marg25bot" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <b>Donate</b><br>
  Here you can see all Paysafecard donations.
</div>

<button type="button" id="loadDonatesPSC" data-loading-text="Loading..." class="btn btn-primary" autocomplete="off">Load Donates</button>
<div class="clear marg25bot"></div>
<div id="showDonatesPSC"></div>