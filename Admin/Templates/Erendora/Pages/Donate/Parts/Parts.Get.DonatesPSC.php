<div style="overflow:scroll;max-height:500px;">
	<table class="table table-bordered">
		<thead>
			<td><b>ID</b></td>
			<td><b>Code</b></td>
			<td><b>Value</b></td>
			<td><b>Date</b></td>
			<td><b>User</b></td>
			<td><b>Checked</b></td>
		</thead>
		<?php 
		
			for($i = 1;$i <= $object['Rows'];$i++){
				echo	'<tr>
							<td>' . $object['Result'][$i]['id'] . '</td>
							<td>' . $object['Result'][$i]['code'] . '</td>
							<td>' . $object['Result'][$i]['value'] . '</td>
							<td>' . date('D d.M.Y', $object['Result'][$i]['date']) . ' GB</td>
							<td>' . $object['Result'][$i]['account'] . '</td>
							<td id="donatestate' . $i . '">';
							
							if((int)$object['Result'][$i]['checked'] === 1){
								echo '<button type="button" id="delete' . $i . '" class="btn btn-danger">Delete</button>';
							}else {
								echo '<button type="button" id="active' . $i . '" class="btn btn-success">Set</button>';
							}
							
				echo		'</td>
						</tr>
						<script>
							$(\'#active' . $i . '\').on(\'click\', function(e) {
									e.preventDefault();
									var formData = {
										\'id\'  : \'' . $object['Result'][$i]['id'] . '\',
										\'account\'  : \'' . $object['Result'][$i]['account'] . '\',
										\'value\'  : \'' . $object['Result'][$i]['value'] . '\',
									};
									var $btn = $(this).button(\'loading\');
									$.ajax({
										type        : "POST",
										url         : "Templates/Erendora/Pages/Donate/Ajax/Ajax.Change.State.PSC.php", 
										data        : formData,
										beforeSend: function() {
											// setting a timeout
											$(\'#donatestate' . $i . '\').html(\'<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>\');
										},
									}).done(function(data) {
										$(\'#donatestate' . $i . '\').html(data).hide();
										$("#donatestate' . $i . '").fadeIn(250);
										$btn.button(\'reset\');
									});
									
								});
								$(\'#delete' . $i . '\').on(\'click\', function(e) {
									e.preventDefault();
									var formData = {
										\'id\'  : \'' . $object['Result'][$i]['id'] . '\',
									};
									var $btn = $(this).button(\'loading\');
									$.ajax({
										type        : "POST",
										url         : "Templates/Erendora/Pages/Donate/Ajax/Ajax.Delete.Donation.PSC.php", 
										data        : formData,
										beforeSend: function() {
											// setting a timeout
											$(\'#donatestate' . $i . '\').html(\'<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>\');
										},
									}).done(function(data) {
										$(\'#donatestate' . $i . '\').html(data).hide();
										$("#donatestate' . $i . '").fadeIn(250);
										$btn.button(\'reset\');
									});
									
								});
							</script>';
			}
		
		?>
	</table>
</div>