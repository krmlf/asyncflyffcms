<div class="alert alert-warning alert-dismissible marg25bot" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <b>Item Wiki</b><br>
  Search for items and list them quickly. If you want, you can press "Add to shop" and the choosed item will be directly added to the shop.
</div>
<?php 
  $itemDBF = new Items();
  $itemDBFcount = $itemDBF->CountFullItemDBF();
?>
<select id="itemRows" class="form-control col-md-3" style="width:200px;margin-right:25px;">
  <option value="0"><?= $itemDBFcount / 2 ?></option>
  <option value="1"><?= $itemDBFcount ?></option>
</select>
<button type="button" id="getFullItemList" data-loading-text="Loading..." class="btn btn-primary" autocomplete="off">Load Itemlist</button>
<div class="clear marg25top marg25bot"></div>
<div class="input-group marg25top marg25bot">
  <input type="text" id="itemValue" class="form-control" placeholder="Type in an item ID">
  <span class="input-group-btn">
      <button id="searchItemByCredentials" class="btn btn-primary" type="button">Search item</button>
  </span>
</div>


<div id="showItemResult"></div>