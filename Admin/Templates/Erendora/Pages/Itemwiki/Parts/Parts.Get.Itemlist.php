<div id="checkShopAddingItem"></div>
<div style="overflow:scroll;max-height:500px;">
	<table class="table table-hover">
		<thead>
			<td><b>Index</b></td>
			<td><b>Icon</b></td>
			<td><b>Item</b></td>
			<td><b>Gender</b></td>
			<td><b>Class</b></td>
			<td><b>Handed</b></td>
			<td><b>Level</b></td>
			<td><b>Add</b></td>
		</thead>
		<?php 
			switch((int)$_POST['itemRows']){
				case 0:
					$start = 1;
					$end = count($object) / 2;
					break;
				case 1: 
					$start = count($object) / 2;
					$end = count($object);
					break;
			}
			
			
			for($i = $start;$i <= $end;$i++){
				echo	'<tr>
							<td class="col-md-1">#' . $object[$i]['Index'] . '</td>
							<td class="col-md-3">' . $object[$i]['szName'] . '</td>
							<td class="col-md-1"><img data-toggle="tooltip" src="' . DEFAULT_TEMPLATE . 'Assets/img/Icons/Items/' . $object[$i]['szIcon'] . '" title="' . $object[$i]['szName'] . '"></td>
							<td class="col-md-1">' . $object[$i]['dwItemSex'] . '</td>';
							
							if($object[$i]['dwItemJob'][0] !== null){
								echo '<td class="col-md-2"><img data-toggle="tooltip" src="' . DEFAULT_TEMPLATE . 'Assets/img/Icons/Classes/' . $object[$i]['JobIcon'] . '.png" title="' . $object[$i]['dwItemJob'][0] . $object[$i]['dwItemJob'][1] . '"></td>';
							}else {
								echo '<td class="col-md-2">' . $object[$i]['dwItemJob'][1] . '</td>';
							}
							
				echo		'<td class="col-md-1">' . $object[$i]['dwHanded'] . '</td>
							<td class="col-md-1">' . $object[$i]['dwItemLV'] . '</td>
							<td class="col-md-1"><button type="button" id="addItemToShopList' . $i . '" data-loading-text="Loading..." class="btn btn-success" autocomplete="off">Add to Shop</button></td>
						</tr>
						<script>
							$(\'#addItemToShopList' . $i . '\').on(\'click\', function(e) {
									e.preventDefault();
									var formData = {
										\'itemid\'  : \'' . $object[$i]['Index'] . '\',
										\'itemname\'  : \'' . $object[$i]['szName'] . '\',
										\'szIcon\'  : \'' . $object[$i]['szIcon'] . '\',
									};
									var $btn = $(this).button(\'loading\');
									$.ajax({
										type        : "POST",
										url         : "Templates/Erendora/Pages/Itemwiki/Ajax/Ajax.AddToShop.php", 
										data        : formData,
										beforeSend: function() {
											// setting a timeout
											$(\'#checkShopAddingItem\').html(\'<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>\');
										},
									}).done(function(data) {
										$(\'#checkShopAddingItem\').html(data).hide();
										$("#checkShopAddingItem").fadeIn(250);
										$btn.button(\'reset\');
									});
									
								});
							</script>';
			}
		?>
	</table>
</div>