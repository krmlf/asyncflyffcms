<div id="checkShopAddingItem"></div>
<div class="row">
	<ul class="list-group col-md-1">
		<li class="list-group-item"><img data-toggle="tooltip" src="<?= DEFAULT_TEMPLATE ?>Assets/img/Icons/Items/<?= $object['Result'][1]['szIcon'] ?>" title="<?= $object['Result'][1]['szName'] ?>"></li>
		<?php 
		
			if($object['Result'][1]['dwItemJob'][0] !== null){
				echo '<li class="list-group-item"><img data-toggle="tooltip" src="' . DEFAULT_TEMPLATE .'Assets/img/Icons/Classes/' . $object['Result'][1]['JobIcon'] . '.png" title="' . $object['Result'][1]['dwItemJob'][0] . $object['Result'][1]['dwItemJob'][1] . '"></li>';
			}else {
				echo '<li class="list-group-item">' .$object['Result'][1]['dwItemJob'][1] . '</b></li>';
			}
		
		?>
	</ul>
	<ul class="list-group col-md-4">
		<li class="list-group-item"><i>ID:</i> <b><?= $object['Result'][1]['Index'] ?></b></li>
		<li class="list-group-item"><i>Name:</i> <b><?= $object['Result'][1]['szName'] ?></b></li>
		<li class="list-group-item"><i>Llevel:</i> <b><?= $object['Result'][1]['dwItemLV'] ?></b></li>
	</ul>
	<ul class="list-group col-md-4">
		<li class="list-group-item"><i>Handed:</i> <b><?= $object['Result'][1]['dwHanded'] ?></b></li>
		<li class="list-group-item"><i>Gender:</i> <b><?= $object['Result'][1]['dwItemSex'] ?></b></li>
		<li class="list-group-item"><button type="button" id="SingleItemToShop" data-loading-text="Loading..." class="btn btn-success" autocomplete="off">Add to Shop</button></li>
	</ul>
</div>
<script>
	$('#SingleItemToShop').on('click', function(e) {
		e.preventDefault();
		var formData = {
			'itemid'  : "<?= $object['Result'][1]['Index'] ?>",
			'itemname'  : "<?= $object['Result'][1]['szName'] ?>",
			'szIcon'  : "<?= $object['Result'][1]['szIcon'] ?>",
		};
		var $btn = $(this).button('loading');
		$.ajax({
			type        : "POST",
			url         : "Templates/Erendora/Pages/Itemwiki/Ajax/Ajax.AddToShop.php", 
			data        : formData,
			beforeSend: function() {
				// setting a timeout
				$('#checkShopAddingItem').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
			},
		}).done(function(data) {
			$('#checkShopAddingItem').html(data).hide();
			$("#checkShopAddingItem").fadeIn(250);
			$btn.button('reset');
		});
		
	});
</script>