<div class="clear marg25top marg25bot"></div>
<div id="insertedkey"></div>

<div class="col-md-12">
	<label>Security Key</label>
    <div class="input-group marg25top marg25bot">
        <select class="form-control" id="setKeyAuthority">
            <option value="Z">Administrator</option>
            <option value="P">Developer</option>
            <option value="O">Gamemaster</option>
        </select>
	</div>
	<div class="input-group marg25top marg25bot">
	<input type="text" id="newSecuritykey" class="form-control" value="<?=  $object ?>" readonly>
	<span class="input-group-btn">
		<button id="saveSecurityKey" class="btn btn-primary" type="button">Save</button>
	</span>
	</div>
</div>

<script>
	$('#saveSecurityKey').on('click', function(e) {
        e.preventDefault();
        var $btn = $(this).button('loading');
		var formData = {
            'newSecuritykey'  : $('#newSecuritykey').val(),
            'setKeyAuthority'  : $('#setKeyAuthority').val(),
        };
        $.ajax({
            type        : "POST",
            url         : "Templates/Erendora/Pages/Securitykeygen/Ajax/Ajax.Insert.Key.php", 
            data        : formData,
            beforeSend: function() {
                // setting a timeout
                $('#insertedkey').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
            },
        }).done(function(data) {
              $('#insertedkey').html(data).hide();
              $("#insertedkey").fadeIn(250);
              $btn.button('reset');
        });
        
    });
</script>