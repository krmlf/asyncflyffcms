<div class="alert alert-warning alert-dismissible marg25bot" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <b>Generate a securitykey</b><br>
  Create a security which response the real authority of a user.
</div>

<button type="button" id="generateSecurityKey" data-loading-text="Loading..." class="btn btn-primary" autocomplete="off">Generate Security Key</button>
<button type="button" id="getAllSecuritykeys" data-loading-text="Loading..." class="btn btn-primary" autocomplete="off">Show all Keys</button>

<div id="showSecurityKey"></div>