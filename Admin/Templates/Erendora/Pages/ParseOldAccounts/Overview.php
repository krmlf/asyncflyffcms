<div class="alert alert-warning alert-dismissible marg25bot" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <b>Parse all old ingame accounts</b><br>
  Parse all old ingame accounts to make it possible that old user can add their old ingame accounts to their website accounts.<br>
  <b>!!!!!!!!! NOTE THAT YOU SHOULD DO IT ONLY ONCE PER TIME WHEN YOUR SSERVER IS ALREADY ONLINE !!!!!!!!!!</b>
</div>

<button type="button" id="loadParseAccountRows" data-loading-text="Loading..." class="btn btn-primary" autocomplete="off">
  Parse old accounts
</button>

<div id="getParseAccountRows"></div>