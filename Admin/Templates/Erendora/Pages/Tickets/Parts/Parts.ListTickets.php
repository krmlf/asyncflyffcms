<div class="tab-content support">
	<div role="tabpanel" class="tab-panel active">
		<table class="table">
			<thead>
				<td><b>ID</b></td>
				<td><b>Title</b></td>
				<td><b>Date</b></td>
				<td><b>Moderator</b></td>
				<td><b>State</b></td>
				<td><b>Replies</b></td>
			</thead>
			<?php 
			for($i = 1;$i <= $object['Rows'];$i++){
				echo	'<tr>
							<td>' . $object['Result'][$i]['id'] . '</td>
							<td><a id="getSingleTicket' . $object['Result'][$i]['id'] .'">' . $object['Result'][$i]['title'] . '</a></td>
							<td>' . date('D d.M.Y', $object['Result'][$i]['date']) . '</td>
							<td>' . $object['Result'][$i]['moderator'] . '</td>
							<td class="' . strtolower(str_replace(' ', '', $object['Result'][$i]['state'])) . '">' . $object['Result'][$i]['state'] . '</td>
							<td><span class="label label-primary">' . $object['Result'][$i]['comments'] . '</span></td>
						</tr>
						<script>
							$(\'#getSingleTicket' . $object['Result'][$i]['id'] .'\').on(\'click\', function(e) {
									e.preventDefault();
									var formData = {
										\'ticketid\'  : \'' . $object['Result'][$i]['id'] .'\',
									};
									var $btn = $(this).button(\'loading\')
									$.ajax({
										type        : "POST",
										url         : "Templates/Erendora/Pages/Tickets/Ajax/Ajax.SingleTicket.php", 
										data        : formData,
										beforeSend: function() {
											// setting a timeout
											$(\'#getTickets\').html(\'<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>\');
										},
									}).done(function(data) {
										$(\'#getTickets\').html(data).hide();
										$("#getTickets").fadeIn(250);
										$btn.button(\'reset\');
									});
									
								});
							</script>';
			}
			?>
		</table>
	</div>
</div>
