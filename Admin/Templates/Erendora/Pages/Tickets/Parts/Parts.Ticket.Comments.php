<?php 
for($i = 1;$i <= count($object['Result']);$i++){
	echo	'<div class="col-md-12">
				<ul class="list-group support">
					<li class="list-group-item ' . strtolower(Flyff::GetAuthorityName($object['Result'][$i]['authority'])) . '">
						<p  style="padding:5px;overflow:auto;">' . nl2br($object['Result'][$i]['comment']) . '</p>
						<p><small><i>Written by <b>' . $object['Result'][$i]['author'] . '</b> on ' . date('D d.M.Y', $object['Result'][$i]['date'])  . '</i></small></p>
					</li>
				</ul>
			</div>';
}