<div class="alert alert-info" role="alert">
	<b>Support</b><br>
	Here you can create a support ticket to share your desire with the <b>Erendora Team</b>.
</div>
<!-- Nav tabs -->
<ul class="nav nav-tabs marg25bot" role="tablist">
	<li role="presentation" class="active"><a id="loadTicket" href="#" aria-controls="events" role="tab" data-toggle="tab">Open</a></li>
</ul>

<div id="getTickets"></div>