<?php 
session_start();
require_once($_SERVER['DOCUMENT_ROOT'].'/Admin/Source/Includes/Config.php');
$controller = new Downloads();
$object = $controller->ChangeState($_POST);
if((int)$object === 0){
	echo '<button type="button" id="active' . (int)$_POST['count'] . '" class="btn btn-danger">Deactive</button>';
}else {
	echo '<button type="button" id="active' . (int)$_POST['count'] . '" class="btn btn-success">Active</button>';
}
echo '<script>
$(\'#active' . (int)$_POST['count'] . '\').on(\'click\', function(e) {
		e.preventDefault();
		var formData = {
			\'id\'  : \'' . $_POST['id'] . '\',
			\'state\'  : \'' . $object . '\',
			\'count\'  : \'' . (int)$_POST['count'] . '\',
		};
		var $btn = $(this).button(\'loading\');
		$.ajax({
			type        : "POST",
			url         : "Templates/Erendora/Pages/Downloads/Ajax/Ajax.Change.State.php", 
			data        : formData,
			beforeSend: function() {
				// setting a timeout
				$(\'#mirrorstate' . (int)$_POST['count'] . '\').html(\'<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>\');
			},
		}).done(function(data) {
			$(\'#mirrorstate' . (int)$_POST['count'] . '\').html(data).hide();
			$("#mirrorstate' . (int)$_POST['count'] . '").fadeIn(250);
			$btn.button(\'reset\');
		});
		
	});
</script>';