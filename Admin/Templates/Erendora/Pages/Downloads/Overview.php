<div class="alert alert-warning alert-dismissible marg25bot" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <b>Downloads</b><br>
  Load all downloads, manage them and add some new mirrors.
</div>

<button type="button" id="loadDownloads" data-loading-text="Loading..." class="btn btn-primary" autocomplete="off">Load Downloads</button>
<button type="button" id="addDownloads" data-loading-text="Loading..." class="btn btn-success" autocomplete="off">Add Downloads</button>
<div class="clear marg25bot"></div>
<div id="showDownloads"></div>