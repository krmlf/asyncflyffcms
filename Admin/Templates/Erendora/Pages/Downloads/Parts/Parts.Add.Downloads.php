<div class="clear marg25bot"></div>
<form method="POST">
  
  <div class="form-group col-md-6">
    <label>Description</label>
    <input type="text" class="form-control" id="description">
  </div>
  <div class="clear"></div>
  
  <div class="form-group col-md-6">
    <label>Mirror</label>
    <input type="text" class="form-control" id="mirror">
  </div>
  <div class="clear"></div>
  
  <div class="form-group col-md-6">
    <label>Size in MB</label>
    <input type="text" class="form-control" id="size">
  </div>
  <div class="clear"></div>
  
  <div class="form-group col-md-6">
    <label>Host</label>
    <input type="text" class="form-control" id="host">
  </div>
  <div class="clear"></div>
  
  <div class="form-group col-md-6">
      <button type="submit" id="addNewMirror" class="btn btn-success">Add</button>
  </div>
</form>

<script>
$('#addNewMirror').on('click', function(e) {
      e.preventDefault();
      var formData = {
          'description'  : $('#description').val(),
          'mirror'  : $('#mirror').val(),
          'host'  : $('#host').val(),
          'size'  : $('#size').val(),
      };
      var $btn = $(this).button('loading');
      $.ajax({
          type        : "POST",
          url         : "Templates/Erendora/Pages/Downloads/Ajax/Ajax.Add.Downloads.php", 
          data        : formData,
          beforeSend: function() {
              // setting a timeout
              $('#showDownloads').html('<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>');
          },
      }).done(function(data) {
            $('#showDownloads').html(data).hide();
            $("#showDownloads").fadeIn(250);
            $btn.button('reset');
      });
      
  });
</script>