<div style="overflow:scroll;max-height:500px;">
	<table class="table table-bordered">
		<thead>
			<td><b>ID</b></td>
			<td><b>Description</b></td>
			<td><b>Mirror</b></td>
			<td><b>Size</b></td>
			<td><b>Date</b></td>
			<td><b>Host</b></td>
			<td><b>State</b></td>
		</thead>
		<?php 
		
			for($i = 1;$i <= $object['Rows'];$i++){
				echo	'<tr>
							<td>' . $object['Result'][$i]['id'] . '</td>
							<td>' . $object['Result'][$i]['description'] . '</td>
							<td>' . $object['Result'][$i]['mirror'] . '</td>
							<td>' . round($object['Result'][$i]['size'] / 1000, 1) . ' GB</td>
							<td>' . date('D d.M.Y', $object['Result'][$i]['registered']) . '</td>
							<td>' . $object['Result'][$i]['host'] . '</td>
							<td id="mirrorstate' . $i . '">';
							
							if((int)$object['Result'][$i]['active'] === 0){
								echo '<button type="button" id="active' . $i . '" class="btn btn-danger">Deactive</button>';
							}else {
								echo '<button type="button" id="active' . $i . '" class="btn btn-success">Active</button>';
							}
							
				echo		'</td>
						</tr>
						<script>
							$(\'#active' . $i . '\').on(\'click\', function(e) {
									e.preventDefault();
									var formData = {
										\'id\'  : \'' . $object['Result'][$i]['id'] . '\',
										\'state\'  : \'' . $object['Result'][$i]['active'] . '\',
										\'count\'  : \'' . $i . '\',
									};
									var $btn = $(this).button(\'loading\');
									$.ajax({
										type        : "POST",
										url         : "Templates/Erendora/Pages/Downloads/Ajax/Ajax.Change.State.php", 
										data        : formData,
										beforeSend: function() {
											// setting a timeout
											$(\'#mirrorstate' . $i . '\').html(\'<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>\');
										},
									}).done(function(data) {
										$(\'#mirrorstate' . $i . '\').html(data).hide();
										$("#mirrorstate' . $i . '").fadeIn(250);
										$btn.button(\'reset\');
									});
									
								});
							</script>';
			}
		
		?>
	</table>
</div>