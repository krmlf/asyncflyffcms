<div class="alert alert-warning alert-dismissible marg25bot" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <b>How to us Webconfig Editor</b><br>
  Load the config file and edit it. After editing just save values with one click on the right of each input field.
</div>

<button type="button" id="loadWebconfigfile" data-loading-text="Loading..." class="btn btn-primary" autocomplete="off">
  Load Web Config
</button>

<div id="showWebConfigfile"></div>