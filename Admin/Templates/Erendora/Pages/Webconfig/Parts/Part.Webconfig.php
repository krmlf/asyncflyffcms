<div class="marg25top marg25bot"></div>
<div class="clear"></div>
<div id="insertedConfig"></div>
<?php

echo Common::Alert2('success', 'Webconfig was sucessfully loaded.');

foreach($object as $key => $value){
  echo '<div class="col-md-6">
          <label data-toggle="tooltip" title="' . $value['description'] . '">' . $key . '</label>
          <div class="input-group marg25top marg25bot">
          <input type="text" id="' . $key . '" class="form-control" value="' . $value['string'] . '">
            <span class="input-group-btn">
              <button id="saveWebconfigValue' . $key . '" class="btn btn-primary" type="button">Save</button>
            </span>
          </div>
        </div>
        <script>
          $(\'#saveWebconfigValue' . $key . '\').on(\'click\', function(e) {
                e.preventDefault();
                var formData = {
                    \'setting\'  : \'' . $key . '\',
                    \'string\'  : $(\'#' . $key . '\').val(),
                };
                var $btn = $(this).button(\'loading\')
                $.ajax({
                    type        : "POST",
                    url         : "Templates/Erendora/Pages/Webconfig/Ajax/Ajax.updateConfig.php", 
                    data        : formData,
                    beforeSend: function() {
                        // setting a timeout
                        $(\'#insertedConfig\').html(\'<div class="pad15"><span style="color:#bbb; font-style:italic;"><img src="Templates/Erendora/Assets/js/ajax-loader.gif" title="">Loading...</span></div>\');
                    },
                }).done(function(data) {
                      $(\'#insertedConfig\').html(data).hide();
                      $("#insertedConfig").fadeIn(250);
                      $btn.button(\'reset\');
                });
                
            });
        </script>';
}
?>