<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
          <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span> Overview
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse  <?= Nav::Collapse('Overview'); ?>" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <div class="list-group">
          <a href="<?= Common::CreateUrl('Dashboard').Common::CreateSection('Overview'); ?>" class="list-group-item <?= Nav::Link('Dashboard'); ?>">Dashboard</a>
        </div>
      </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Account
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse <?= Nav::Collapse('Account'); ?>" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
        <div class="list-group">
          <a href="<?= Common::CreateUrl('Searchaccount').Common::CreateSection('Account'); ?>" class="list-group-item <?= Nav::Link('Searchaccount'); ?>">Search Acount</a>
        </div>
      </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
         <span class="glyphicon glyphicon-tag" aria-hidden="true"></span> Item
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse <?= Nav::Collapse('Item'); ?>" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
       <div class="list-group">
          <a href="<?= Common::CreateUrl('Itemwiki').Common::CreateSection('Item'); ?>" class="list-group-item <?= Nav::Link('Itemwiki'); ?>">Item Wiki</a>  
          <a href="<?= Common::CreateUrl('Shoplist').Common::CreateSection('Item'); ?>" class="list-group-item <?= Nav::Link('Shoplist'); ?>">Shop List</a>
        </div>
      </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingSupport">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSupport" aria-expanded="false" aria-controls="collapseSupport">
         <span class="glyphicon glyphicon-tag" aria-hidden="true"></span> Support
        </a>
      </h4>
    </div>
    <div id="collapseSupport" class="panel-collapse collapse <?= Nav::Collapse('Support'); ?>" role="tabpanel" aria-labelledby="headingSupport">
      <div class="panel-body">
       <div class="list-group">
          <a href="<?= Common::CreateUrl('Tickets').Common::CreateSection('Support'); ?>" class="list-group-item <?= Nav::Link('Tickets'); ?>">Tickets</a>  
        </div>
      </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFour">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          <span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Security
        </a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse <?= Nav::Collapse('Security'); ?>" role="tabpanel" aria-labelledby="headingFour">
      <div class="panel-body">
        <div class="list-group">
          <a href="<?= Common::CreateUrl('Securitykeygen').Common::CreateSection('Security'); ?>" class="list-group-item <?= Nav::Link('Securitykeygen'); ?>">Securitykey Generator</a>
        </div>
      </div>
    </div>
  </div>
  
   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFive">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
          <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span> Settings
        </a>
      </h4>
    </div>
    <div id="collapseFive" class="panel-collapse collapse <?= Nav::Collapse('Settings'); ?>" role="tabpanel" aria-labelledby="headingFive">
      <div class="panel-body">
        <div class="list-group">
          <a href="<?= Common::CreateUrl('Webconfig').Common::CreateSection('Settings'); ?>" class="list-group-item <?= Nav::Link('Webconfig'); ?>">Webconfig Editor</a>
        </div>
      </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingMisc">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseMisc" aria-expanded="false" aria-controls="collapseMisc">
          <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> Misc
        </a>
      </h4>
    </div>
    <div id="collapseMisc" class="panel-collapse collapse <?= Nav::Collapse('Misc'); ?>" role="tabpanel" aria-labelledby="headingMisc">
      <div class="panel-body">
        <div class="list-group">
          <a href="<?= Common::CreateUrl('Downloads').Common::CreateSection('Misc'); ?>" class="list-group-item <?= Nav::Link('Downloads'); ?>">Downloads</a>
          <a href="<?= Common::CreateUrl('Donate').Common::CreateSection('Misc'); ?>" class="list-group-item <?= Nav::Link('Donate'); ?>">Donate</a>
          <a href="<?= Common::CreateUrl('ParseOldAccounts').Common::CreateSection('Misc'); ?>" class="list-group-item <?= Nav::Link('ParseOldAccounts'); ?>" style="color:red;">Parse old accounts</a>
        </div>
      </div>
    </div>
  </div>
  
</div>