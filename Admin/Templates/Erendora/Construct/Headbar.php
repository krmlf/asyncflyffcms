<nav class="navbar navbar-primary">
  <div class="container-fluid">

    <div class="row">
      <div class="col-md-5">
        <a class="navbar-brand" href="#"><?= $cfg['web_title'] ?> - <?= $cfg['web_desc_panel'] ?> v<?= $cfg['admincp_version'] ?></a>
      </div>
      
      <div class="col-md-7">
        <p class="navbar-text navbar-right">Signed in as <a href="#" class="navbar-link"><?= $_SESSION['user'] ?></a></p>
      </div>
      
    </div>

  </div>
</nav>