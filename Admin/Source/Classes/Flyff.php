<?php 

/**
* All flyff specific methods
**/

class Flyff {
    
    public static function DefineHandedItem($dwHanded){
        switch($dwHanded){
			case 1: 
				$dwHanded = 'One Handed';
				break;
			case 2: 
				$dwHanded = 'Two Handed';
				break;
			default: 
				$dwHanded = 'No Handed';
				break;
		}
        return $dwHanded;
    }
	
    public static function DefineItemGender($dwItemSex){
		switch($dwItemSex){
			case 0: 
				$dwItemSex = 'Male';
				break;
			case 1: 
				$dwItemSex = 'Femaile';
				break;
			default: 
				$dwItemSex = 'Unisex';
				break;
		}
        return $dwItemSex;
	}
    
    public static function GetAuthorityName($authority){
        $array = array(
            'F'     =>      null,
            'O'     =>      'Gamemaster',
            'P'     =>      'Developer',
            'Z'     =>      'Administrator',  
        );
        return $array[$authority];
    }
    
	public static function GetClassName($jobName){
        if(intval($jobName) !== -1){
            switch($jobName){
            //1-15
            case 0:
                $jobName = array('Vagrant', false);
                break;
            //First Job 15-60
            case 1:
                $jobName = array('Mercenary', false);
                break;
            case 2:
                $jobName = array('Acrobat', false);
                break;
            case 3:
                $jobName = array('Assist', false);
                break;
            case 4:
                $jobName = array('Magician', false);
                break;
            case 5:
                $jobName = array('Puppeteer', false);
                break;
            //Second Job 60-129
            case 6:
                $jobName = array('Knight', false);
                break;
            case 7:
                $jobName = array('Blade', false);
                break;
            case 8:
                $jobName = array('Jester', false);
                break;
            case 9:
                $jobName = array('Ranger', false);
                break;
            case 10:
                $jobName = array('Ringmaster', false);
                break;
            case 11:
                $jobName = array('Billposter', false);
                break;
            case 12:
                $jobName = array('Psykeeper', false);
                break;
            case 13:
                $jobName = array('Elementor', false);
                break;
            case 14:
                $jobName = array('Gatekeeper', false);
                break;
            case 15:
                $jobName = array('Doppler', false);
                break;
            //Master
            case 16:
                $jobName = array('Knight', 'Master');
                break;
            case 17:
                $jobName = array('Blade', 'Master');
                break;
            case 18:
                $jobName = array('Jester', 'Master');
                break;
            case 19:
                $jobName = array('Ranger', 'Master');
                break;
            case 20:
                $jobName = array('Ringmaster', 'Master');
                break;
            case 21:
                $jobName = array('Billposter', 'Master');
                break;
            case 22:
                $jobName = array('Psykeeper', 'Master');
                break;
            case 23:
                $jobName = array('Elementor', 'Master');
            //Hero
            case 24:
                $jobName = array('Knight', 'Hero');
                break;
            case 25:
                $jobName = array('Blade', 'Hero');
                break;
            case 26:
                $jobName = array('Jester', 'Hero');
                break;
            case 27:
                $jobName = array('Ranger', 'Hero');
                break;
            case 28:
                $jobName = array('Ringmaster', 'Hero');
                break;
            case 29:
                $jobName = array('Billposter', 'Hero');
                break;
            case 30:
                $jobName = array('Psykeeper', 'Hero');
                break;
            case 31:
                $jobName = array('Elementor', 'Hero');
                break;
            //Legend
            case 32:
                $jobName = array('Templer', false);
                break;
            case 33:
                $jobName = array('Slayer', false);
                break;
            case 34:
                $jobName = array('Harlequin', false);
                break;
            case 35:
                $jobName = array('Crackshooter', false);
                break;
            case 36:
                $jobName = array('Seraph', false);
                break;
            case 37:
                $jobName = array('Forcemaster', false);
                break;
            case 38:
                $jobName = array('Mentalist', false);
                break;
            case 39:
                $jobName = array('Arcanist', false);
                break;
                
            }
        }else {
            $jobName = array(null, 'All Jobs');
        }
        
        return $jobName;
    }
    
    
	
}