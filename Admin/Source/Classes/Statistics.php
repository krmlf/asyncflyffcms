<?php

class Statistics {
	
    private $value, $rate;
    
	function __construct($value, $rate = 1) {
		$this->value = $value;
        $this->rate = $rate;
	}
    
    //Anzahl der Values
    public function ValueCount($count = 0){
        foreach($this->rate as $val){
            $count++;
        }
        return $count;
    }
    
    //Summe der Häufigkeiten
    public function RateSum($sum = 0){
        foreach($this->rate as $val){
            $sum += $val;
        }
        return $sum;
    }
    
    public function Average($math = array(), $average = 0){
        for($i = 0;$i < self::ValueCount();$i++){
            $math[$i] = $this->value[$i] * $this->rate[$i];
        }
        foreach($math as $val){
            $average += $val;
        }
        return $average / self::RateSum() ;
        
    }
    
    public function Varianz($math = array(), $sum = 0, $varianz = 0){
        for($i = 0;$i < self::ValueCount();$i++){
            $math[$i] = self::Average() - $this->value[$i];
        }
        foreach($math as $key => $val){
            $math[$key] = $val*$val;
        }
        foreach($math as $val){
            $sum += $val;
        }
        $varianz = $sum / (self::RateSum() - 1);
        return $varianz;
    }
    
    public function Strict($strict = 0){
        $strict = sqrt(self::Varianz());
        return $strict;
    }
    
    
}

