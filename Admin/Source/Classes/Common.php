<?php


class Common extends Database {
	
    public function RealAccess($sessionKey = 'id'){
        if(isset($_SESSION[$sessionKey])){
            if($_SESSION['authority'] === 'Z' or $_SESSION['authority'] === 'P' or $_SESSION['authority'] === 'O'){
                if(isset($_SESSION['panelkey']) && !empty($_SESSION['panelkey'])){
                    $getKey = self::Select(array('password'), 'Website', 'Securitykey', array('authority' => $_SESSION['authority']));
                    if($getKey['Rows'] > 0){
                        $getKey = $getKey['Result'][1]['password'];
                        if($getKey === $_SESSION['panelkey']){
                            $getUserTrys = self::Select(array('trys'), 'Website', 'WebAccount', array('username' => $_SESSION['user']));
                            if($getUserTrys['Rows'] > 0){
                                $getUserTrys = $getUserTrys['Result'][1]['trys'];
                                if($getUserTrys <= 3){
                                    return 1;
                                }else {
                                    return 0;
                                }
                            }else {
                                return 0;
                            }
                        }else {
                            return 0;
                        }
                    }else {
                        return 0;
                    }
                }else {
                    return 0;
                }
            }else {
                return 0;
            }
        }else {
            return 0;
        }
	}
    
    public static function ValidateInput($array){
        foreach($array as $key => $value){
            $array[$key] = htmlspecialchars($value);
            $array[$key] = htmlentities($value);
            $array[$key] = strip_tags($value);
        }
        return $array;
    }
    
    public static function DynamicRefresh($site, $bool = true){
        if($bool){echo '<script type="text/javascript">window.location = "' . $site . '";</script>';}
    }
    
    public static function InputCheck($array){
        $result = true;
        foreach($array as $key => $value){
            if(empty($array[$key])){
                $result = false;
                break;
            }
        }
        return $result;
    }
    
    public static function PostCheck($array){
        $result = true;
        foreach($array as $key => $value){
            if(!isset($array[$key])){
                $result = false;
                break;
            }
        }
        return $result;
    }
    
    public static function Alert($key, $value){
        $alertkey = $key;
        $alertvalue = $value;
        $rq = require_once(TEMPLATE_PATH . DEFAULT_TEMPLATE . '/Construct/Alert.php');
    }
    
    public static function Alert2($key, $value){
        $out = '<div class="alert alert-' . $key . '">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ' . nl2br(utf8_encode($value)) . '
                </div>';
        return $out;
    }
    
    public static function Logout(){
        if(isset($_GET['logout'])){
            session_destroy();
            echo '<script type="text/javascript">window.location = "' . self::CreateUrl(DEFAULT_SITE) . '";</script>';
        }
    }
    
    public static function GetCookie($param){
        if(isset($_COOKIE[$param])){
            if(!empty($_COOKIE[$param])){
                return $_COOKIE[$param];
            }
        }
    }
    
    public static function CreateUrl($value){
        return '?p='.bin2hex($value);
    }
    
    public static function CreateSection($value){
        return '&sp='.bin2hex($value);
    }
    
}
