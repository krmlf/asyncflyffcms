<?php 

class Database {
	
	private $sql, $database;
	
	function __construct(){
		
		global $mssqlCredentialsArray;
		
		$this->sql = @odbc_connect('Driver={SQL Server};Server=' . 
			$mssqlCredentialsArray['host'] . ';Port=1433',
            $mssqlCredentialsArray['user'],
            $mssqlCredentialsArray['pass']
		);
			
		$this->database = array(
            'Account'   =>      'ACCOUNT_DBF',
            'Character' =>      'CHARACTER_01_DBF',
            'Item'      =>      'ITEM_DBF',
            'Ranking'   =>      'RANKING_DBF',
            'Log'       =>      'LOGGING_01_DBF',
            'Website'   =>      'WEBSITE_DBF'
        );
	}
    
    //Binary to hexadecimal
    private function Encrypt($value){
        $value = '0x'.bin2hex($value);
        return $value;
    }
    
    //Hexadecimal to binary
    private function Decrypt($value){
        $value = hex2bin(str_replace('0x', '',  $value));
        return $value;
    }
    
    //Validates an object
    private function ObjectSecure($object){
        if(is_array($object)){
            foreach($object as $key => $value){
                if(gettype($value) != 'integer'){
                    if(substr($value, 0, 2) !== '0x'){
                        $object[$key] = self::Encrypt($value);
                    }else {
                        $object[$key] = '\''.self::Decrypt($value).'\'';
                    }
                }
            }
        }/**else {
            if(gettype($object) != 'integer'){
                $object = self::Encrypt($object);
            }
        }*/
        return $object;
    }
    //Makes an object readable
    private function ObjectRead($object){
        if(is_array($object)){
            foreach($object as $key => $value){
                if(gettype($value) != 'integer'){
                    $object[$key] = self::Decrypt($value);
                }
            }
        }/**else {
            if(gettype($object) != 'integer'){
                $object = self::Decrypt($object);
            }
        }*/
        return $object;
    }
    
    //Queryconstruction
    private function QueryConstruction($escapedInput, $queryType, $whereException = '='){
        $queryRows = null;
        
        switch($queryType){
            case 'rows':
                foreach($escapedInput as $key => $value){
                    $queryRows .= '[' . $key . '], ';
                 } 
                $subQueryRows = substr($queryRows, 0, strlen($queryRows) - 2);
                break;
            case 'rows2':
                foreach($escapedInput as $key => $value){
                    $queryRows .= '[' . $value . '], ';
                 } 
                $subQueryRows = substr($queryRows, 0, strlen($queryRows) - 2);
                break;
            case 'dual':
                $i = 0;
                if(count($escapedInput) > 1){
                    foreach($escapedInput as $key => $value){
                        $queryRows .= '[' . $key . '] ' . $whereException[$i] . ' ' . $value . ' AND ';
                        $i++;
                    }
                }else {
                    foreach($escapedInput as $key => $value){
                        $queryRows .= '[' . $key . '] ' . $whereException . ' ' . $value . ' AND ';
                    }
                }
                $subQueryRows = substr($queryRows, 0, strlen($queryRows) - 5);
                break;
            case 'values':
                foreach($escapedInput as $key => $value){
                   $queryRows .= $value . ', ';
                }
                $subQueryRows = substr($queryRows, 0, strlen($queryRows) - 2);
                break;
        }
        
        return $subQueryRows;
    }
    
    //Execs a query
    private function Exec($preparedQuery){
        return @odbc_exec($this->sql, $preparedQuery);
    }
    
    //Fetchs a query
    private function Fetch($result, $rows, $arr = array()){
        for($i = 1;$i <= $rows;$i++){
           $arr[$i] = odbc_fetch_array($result, $i); 
        }
        return $arr;
    }
    
    //Get count of successfull founded rows
    private function Rows($result){
        return odbc_num_rows($result);
    }
    
    //Update
    protected function Update($mixedInput, $whereValue, $usedDatabase, $table){
        $queryCredentials = self::QueryConstruction(self::ObjectSecure($mixedInput), 'dual');
        $searchCredentials = self::QueryConstruction(self::ObjectSecure($whereValue), 'dual');
        self::Exec('UPDATE [' . $this->database[$usedDatabase] . '].[dbo].[' . $table . '] SET ' . $queryCredentials . ' WHERE ' . $searchCredentials);
        //echo '<br><br>'.'UPDATE [' . $this->database[$usedDatabase] . '].[dbo].[' . $table . '] SET ' . $queryCredentials . ' WHERE ' . $searchCredentials.'<br><br>';
        if(odbc_error()){echo odbc_errormsg($this->sql);}
    }
    
    //Insert
    protected function Insert($mixedInput, $usedDatabase, $table){
        $queryCredentials = self::QueryConstruction(self::ObjectSecure($mixedInput), 'rows');
        $valueCredentials = self::QueryConstruction(self::ObjectSecure($mixedInput), 'values');
        self::Exec('INSERT INTO [' . $this->database[$usedDatabase] . '].[dbo].[' . $table . '] (' . $queryCredentials . ') VALUES (' . $valueCredentials . ')');
        if(odbc_error()){echo odbc_errormsg($this->sql);}
    }
    
    //Select
    protected function Select($mixedInput, $usedDatabase, $table, $mixedWhere = false, $exploit = null, $firstExtension = null, $change = 0, $whereException = '='){
        if($change === 0){
            $queryCredentials = self::QueryConstruction($mixedInput, 'rows2');
        }elseif($change === 1) {
            $queryCredentials = self::QueryConstruction($mixedInput, 'values');
        }
        $whereCredentials = null;
        if($mixedWhere){
            $whereCredentials = ' WHERE ' . self::QueryConstruction(self::ObjectSecure($mixedWhere), 'dual', $whereException);
        }
        $craft = 'SELECT ' . $firstExtension . $queryCredentials . ' FROM [' . $this->database[$usedDatabase] . '].[dbo].[' . $table . ']' . $whereCredentials . $exploit;
        //echo 'SELECT ' . $firstExtension . $queryCredentials . ' FROM [' . $this->database[$usedDatabase] . '].[dbo].[' . $table . ']' . $whereCredentials . $exploit;
        $exec = self::Exec($craft);
        if(odbc_error()){echo odbc_errormsg($this->sql);}
        $rows = self::Rows($exec);
        $fetch = self::Fetch($exec, $rows);
        return array('Result' => $fetch, 'Rows' => $rows);
    }
	
    
    //Delete
    protected function Delete($mixedInput, $usedDatabase, $table){
        $queryCredentials = self::QueryConstruction(self::ObjectSecure($mixedInput), 'dual');
        self::Exec('DELETE FROM [' . $this->database[$usedDatabase] . '].[dbo].[' . $table . '] WHERE ' . $queryCredentials);
        if(odbc_error()){echo odbc_errormsg($this->sql);}
    }
    
    //Prevent spam with recaptcha
    
    protected function PreventSpam($captchaResponse){
		$url = 'https://www.google.com/recaptcha/api/siteverify';
        $privatekey = RECAPTCHA_SECRET_KEY;
        
        $response = file_get_contents($url.'?secret='.$privatekey.'&response='.$captchaResponse);
        $data = json_decode($response);
        if(isset($data->success) AND $data->success == true){
          return true;
        }else {
          return false;
        }
	}
    
}