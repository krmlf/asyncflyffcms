<?php 

$siteLocation = $path . DEFAULT_TEMPLATE . '/Pages/';
$fileType = '.php';

 if(isset($_GET[SITE_PARAM]) && !empty($_GET[SITE_PARAM])) {
   $site = hex2bin($_GET[SITE_PARAM]);
  if(ctype_xdigit($_GET[SITE_PARAM])){
  $site = preg_replace('/[^A-Za-z]/', '', $site);
  if(file_exists($siteLocation . $site . '/Overview' . $fileType)) {
    require_once($siteLocation . $site . '/Overview' . $fileType);
  } 
  else {
    require_once($siteLocation.DEFAULT_SITE. '/Overview' .$fileType);
  }
  }else {
    echo Common::Alert2('danger', 'Url must be a converted string');
  }
}else {
  require_once($siteLocation.DEFAULT_SITE. '/Overview' .$fileType);
}
 
