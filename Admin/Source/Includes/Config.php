<?php

ini_set('display_errors', true);
error_reporting(E_ALL);
date_default_timezone_set('Europe/Berlin');

$path = $_SERVER['DOCUMENT_ROOT'].'/Admin/';

require_once($path.'Source/Includes/Includes.php');

$access =  new Common();
if(!$access->RealAccess()){die('You don\'t have access to this section.');}