<?php 

class Downloads extends Database {
	
	public function AddMirror($post)
	{
		if(!Common::InputCheck($post)){return Common::Alert2('danger', 'Please fill in all given fields.');}
		self::Insert(array('description' => $post['description'], 'mirror' => $post['mirror'], 'size' => (int)$post['size'], 'registered' => time(), 'host' => $post['host']), 'Website', 'Download');
		return Common::Alert2('success', 'A new mirror has been added.');
	}
	
	public function GetMirrors()
	{
		$mirrors = self::Select(array('id', 'description', 'mirror', 'size', 'registered', 'host', 'active'), 'Website', 'Download', null, ' ORDER BY [id] DESC');
		if($mirrors['Rows']){
			$mirrors['bool'] = true;
			return $mirrors;
		}else {
			return array('danger', 'There are actually no downloads.', 'bool' => false);
		}
	}
	
	public function ChangeState($post)
	{
		(int)$post['state'] === 1 ? $post['state'] = 0 : $post['state'] = 1;
		
		self::Update(array('active' => $post['state']), array('id' => (int)$post['id']), 'Website', 'Download');
		return $post['state'];
	}
	
}