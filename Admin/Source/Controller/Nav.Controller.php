<?php

class Nav {
	
    public static function Link($param){
        $arr = array('active', null);
        $site = hex2bin($_GET[SITE_PARAM]);
        if(isset($_GET[SITE_PARAM]) and !empty($_GET[SITE_PARAM])){
             $site = hex2bin($_GET[SITE_PARAM]);
            if($param === $site){
                return $arr[0];
            }else {
                return $arr[1];
            }
        }else {
            if($param === DEFAULT_SITE){
                return $arr[0];
            }else {
                return $arr[1];
            }
        }
    }
    
    public static function Collapse($param){
        $arr = array('in', null);
        if(isset($_GET[SECOND_SITE_PARAM]) and !empty($_GET[SECOND_SITE_PARAM])){
            $site = hex2bin($_GET[SECOND_SITE_PARAM]);
            if($param === $site){
                return $arr[0];
            }else {
                return $arr[1];
            }
        }else {
            if($param === DEFAULT_SITE){
                return $arr[0];
            }else {
                return $arr[1];
            }
        }
    }
    
}
