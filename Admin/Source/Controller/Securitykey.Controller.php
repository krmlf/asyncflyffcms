<?php 

class Securitykey extends Database {
	
	private function SetSecurityKey(){
		global $cfg;
		$mixedValues = $cfg['server_salt_2'].microtime().$cfg['server_salt_1'].uniqid().$cfg['server_salt_3'].time().$cfg['server_salt_2'].rand(1, 1000000000);
		$mixedValues = hash('sha256', md5($mixedValues));
		return $mixedValues;
	}
	
	public function GetSecurityKey(){
		$getKey = self::SetSecurityKey();
		return $getKey;
	}
	
	public function UpdateSecurityKey($post){
		$post = Common::ValidateInput($post);
		self::Update(array('password' => $post['newSecuritykey']), array('authority' => $post['setKeyAuthority']), 'Website', 'SecurityKey');
		return Common::Alert2('success', 'Security key <b>' . $post['newSecuritykey'] . '</b> was saved for authority <b>'.$post['setKeyAuthority'].'</b>.');
	}
	
	public function SelectSecurityKeys(){
		$keys = self::Select(array('password', 'authority'), 'Website', 'SecurityKey');
		$keys = $keys['Result'];
		return $keys;
	}
	
}