<?php 

class Shop extends Database {
	
	private function LoadShopListDBF(){
		$shoplist = self::Select(array('id', 'itemid', 'itemname', 'count', 'price', 'description', 'currency', 'active', 'szIcon', 'category'), 'Website', 'ShopList');
		return $shoplist;
	}
	
	private function LoadShopCategoryDBF(){
		$categorys = self::Select(array('id', 'category'), 'Website', 'ShopCategory', null, ' ORDER BY [id]');
		$categorys = $categorys['Result'];
		return $categorys;
	}
	
	public function GetShopList(){
		$list = self::LoadShopListDBF();
		$count = $list['Rows'];
		$result = $list;
		for($i = 1;$i <= $count;$i++){
			$result['Result'][$i]['szIcon'] = substr($result['Result'][$i]['szIcon'], 0, -4).'.png';
		}
		$result['Categorys'] = self::LoadShopCategoryDBF();
		return $result;
	}
	
	private function ItemKey($itemID){
		$key = $itemID.md5('sortneg').microtime().uniqid().md5('flmrk').time().'nivek'.rand(1, 1000000000);
		$key = hash('sha256', $key);
		return $key;
	}
	
	public function ActivateItem($post){
		$post = Common::ValidateInput($post);
		self::Update(array('active' => intval($post['state'])), array('itemid' => intval($post['itemid'])), 'Website', 'ShopList');
		self::Update(array('secure' => self::ItemKey($post['itemid'])), array('itemid' => intval($post['itemid'])), 'Website', 'ShopList');
		return Common::Alert2('success', 'State of #' . $post['itemid'] . ' has been changed.');
	}
	
	public function DeleteItem($post){
		$post = Common::ValidateInput($post);
		self::Delete(array('itemid' => intval($post['itemid'])), 'Website', 'ShopList');
	}
	
	public function UpdateShopItem($post){
		$post = Common::ValidateInput($post);
		self::Update(array('count' => intval($post['count'])), array('itemid' => intval($post['itemid'])), 'Website', 'ShopList');
		self::Update(array('price' => intval($post['price'])), array('itemid' => intval($post['itemid'])), 'Website', 'ShopList');
		self::Update(array('description' => $post['desc']), array('itemid' => intval($post['itemid'])), 'Website', 'ShopList');
		self::Update(array('currency' => $post['currency']), array('itemid' => intval($post['itemid'])), 'Website', 'ShopList');
		self::Update(array('category' => $post['category']), array('itemid' => intval($post['itemid'])), 'Website', 'ShopList');
		self::Update(array('secure' => self::ItemKey($post['itemid'])), array('itemid' => intval($post['itemid'])), 'Website', 'ShopList');
	}
	
}