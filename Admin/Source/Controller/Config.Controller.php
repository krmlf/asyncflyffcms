<?php 

class Config extends Database {
	
	private function LoadConfig(){
		$configFile = self::Select(array('setting', 'string', 'description'), 'Website', 'WebConfig', null);
		for($i = 1;$i <= $configFile['Rows'];$i++){
			$configOutput[$configFile['Result'][$i]['setting']] = $configFile['Result'][$i];
			$configOutput[$configFile['Result'][$i]['setting']]['string'] = $configOutput[$configFile['Result'][$i]['setting']]['string'];
			$configOutput[$configFile['Result'][$i]['setting']]['description'] = $configOutput[$configFile['Result'][$i]['setting']]['description'];
		}
		return $configOutput;
	}
	
	public function GetConfig(){
		$configFile = self::Select(array('setting', 'string'), 'Website', 'WebConfig', null);
		for($i = 1;$i <= $configFile['Rows'];$i++){
			$configOutput[$configFile['Result'][$i]['setting']] = $configFile['Result'][$i];
			$configOutput[$configFile['Result'][$i]['setting']] = $configOutput[$configFile['Result'][$i]['setting']]['string'];
		}
		return $configOutput;
	}
	
	public function PushNewConfigFile($post){
		$post = Common::ValidateInput($post);
		self::Update(array('string' => $post['string']), array('setting' => $post['setting']), 'Website', 'WebConfig');
		return Common::Alert2('success', 'Setting: '.$post['setting'].' was updated.');
	}
	
	public function GetConfigEditor(){
		$configFile = self::LoadConfig();
		return $configFile;
	}
	
}