<?php 

class Donate extends Database {
	
	public function GetPSCDonates(){
		$donates = self::Select(array('id', 'code', 'value', 'date', 'account', 'checked'), 'Website', 'DonatePSC', null, ' ORDER BY [id] DESC');
		if($donates['Rows'] > 0){
			$donates['bool'] = true;
			return $donates;
		}else {
			return array('danger', 'There are actually no donates found.', 'bool' => false);
		}
	}
	
	public function SetPSCDonatePoints($post){
		global $cfg;
		$pscstate = self::Select(array('checked'), 'Website', 'DonatePSC', array('id' => (int)$post['id']));
		$pscstate = $pscstate['Result'][1]['checked'];
		if((int)$pscstate === 0){
			$oldcoins = self::Select(array('id', 'dcoins'), 'Website', 'WebAccount', array('username' => $post['account']));
			$id = $oldcoins['Result'][1]['id'];
			$dcoins = $oldcoins['Result'][1]['dcoins'];
			$newcoins = (int)$dcoins + $cfg['donate_' . $post['value']];
			self::Update(array('dcoins' => $newcoins), array('id' => (int)$id), 'Website', 'WebAccount');
			self::Update(array('checked' => 1), array('id' => (int)$post['id']), 'Website', 'DonatePSC');
			return array('success', 'Donate coins has been added.');
		}else {
			return array('danger', 'Donate coins are already given to this player.');
		}
	}
	
	public function DeletePSCDonatePoints($post){
		self::Delete(array('id' => (int)$post['id']), 'Website', 'DonatePSC');
		return array('success', 'Donation has been deleted.');
	}
	
}