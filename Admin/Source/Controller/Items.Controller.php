<?php 

class Items extends Database {
	
	private function LoadFullItemDBF(){
		$list = self::Select(array('Index', 'szName', 'dwItemSex', 'dwItemJob', 'dwHanded', 'dwItemLV', 'szIcon'), 'Item', 'ITEM_TBL', null, ' ORDER BY [Index]');
		return $list;
	}
	
	public function CountFullItemDBF(){
		$list = self::Select(array('Index'), 'Item', 'ITEM_TBL');
		return $list['Rows'];
	}
	
	public function AddShopListItem($post){
		$post = Common::ValidateInput($post);
		$checkShopList = self::Select(array('id'), 'Website', 'ShopList', array('itemid' => intval($post['itemid'])));
		if($checkShopList['Rows'] <= 0){
			self::Insert(array('itemid' => intval($post['itemid']), 'itemname' => $post['itemname'], 'szIcon' => $post['szIcon']), 'Website', 'ShopList');
			return Common::Alert2('success', 'Item was added to the shop.');
		}else {
			return Common::Alert2('danger', 'This item is already in the shop list.');
		}
	}
	
	//Search Single Item
	public function GetSingleItemData($post){
		$post = Common::ValidateInput($post);
		$item = self::Select(array('Index', 'szName', 'dwItemSex', 'dwItemJob', 'dwHanded', 'dwItemLV', 'szIcon'), 'Item', 'ITEM_TBL', array('Index' => intval($post['itemValue'])));
		if($item['Rows'] > 0){
			$item['bool'] = true;
			$item['Result'][1]['dwItemSex'] = Flyff::DefineItemGender($item['Result'][1]['dwItemSex']);
			$item['Result'][1]['JobIcon'] = $item['Result'][1]['dwItemJob'];
			$item['Result'][1]['dwItemJob'] = Flyff::GetClassName($item['Result'][1]['dwItemJob']);
			$item['Result'][1]['dwHanded'] = Flyff::DefineHandedItem($item['Result'][1]['dwHanded']);
			if(intval($item['Result'][1]['dwItemLV']) === -1){
				$item['Result'][1]['dwItemLV'] = 'All Levels';
			}
			$item['Result'][1]['szIcon'] = substr($item['Result'][1]['szIcon'], 0, -4).'.png';
			return $item;
		}else {
			$item['bool'] = false;
		}
	}
	
	//Load Full Item List
	
	public function GetFullItemListData(){
		$list = self::LoadFullItemDBF();
		$count = $list['Rows'];
		$result = $list['Result'];
		for($i = 1;$i <= $count;$i++){
			$result[$i]['dwItemSex'] = Flyff::DefineItemGender($result[$i]['dwItemSex']);
			$result[$i]['JobIcon'] = $result[$i]['dwItemJob'];
			$result[$i]['dwItemJob'] = Flyff::GetClassName($result[$i]['dwItemJob']);
			$result[$i]['dwHanded'] = Flyff::DefineHandedItem($result[$i]['dwHanded']);
			if(intval($result[$i]['dwItemLV']) === -1){
				$result[$i]['dwItemLV'] = 'All Levels';
			}
			$result[$i]['szIcon'] = substr($result[$i]['szIcon'], 0, -4).'.png';
		}
		return $result;
	}
	
}